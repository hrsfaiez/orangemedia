package tn.dev.orange.orangemedia.recoveryrequest.dataaccess.dataprovider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tn.dev.orange.orangemedia.media.domain.datamodel.Media;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequestState;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.recoveryrequest.IRecoveryRequestModelProvider;

public class RecoveryRequestModelProvider implements IRecoveryRequestModelProvider {

    public static RecoveryRequestModelProvider newInstance() {
        return new RecoveryRequestModelProvider();
    }


    @Override
    public List<RecoveryRequest> getActiveRecoveryRequests(User user) {
        List<RecoveryRequest> requestsList = new ArrayList<RecoveryRequest>();
        RecoveryRequest recoveryRequest = new RecoveryRequest(new Media("media1REq", new Date()),
                "reesTitle1", "reqCategory1", new Date(), new Date(),
                "reqContent", RecoveryRequestState.notSeen);
        RecoveryRequest recoveryRequestO = new RecoveryRequest(new Media("media1REq", new Date()),
                "reesTitle22", "reqCategory2", new Date(), new Date(),
                "reqContent", RecoveryRequestState.seen_responded);
        RecoveryRequest recoveryRequest1 = new RecoveryRequest(new Media("media1REq", new Date()),
                "reesTitle3", "reqCategory3", new Date(), new Date(),
                "reqContent", RecoveryRequestState.seen_notResponded);
        requestsList.add(recoveryRequest);
        requestsList.add(recoveryRequestO);
        requestsList.add(recoveryRequest1);
        return requestsList;
    }

    @Override
    public void deleteRequest(RecoveryRequest recoveryRequest) {

    }
}
