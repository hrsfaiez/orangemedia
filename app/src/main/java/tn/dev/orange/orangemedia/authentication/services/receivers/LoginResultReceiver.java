package tn.dev.orange.orangemedia.authentication.services.receivers;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.authentication.services.LoginService;

public class LoginResultReceiver extends ResultReceiver {

    private IOnLoginActionComplete onLoginCheckCompleteListener;

    public LoginResultReceiver(Handler handler,
                               IOnLoginActionComplete onLoginCheckCompleteListener) {
        super(handler);
        this.onLoginCheckCompleteListener = onLoginCheckCompleteListener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (null != onLoginCheckCompleteListener) {
            if (LoginService.LOGIN_SUCCESS == resultCode) {
                onLoginCheckCompleteListener.onLoginSuccess();
            } else if (LoginService.LOGIN_FAILURE == resultCode) {
                onLoginCheckCompleteListener.onLoginFailure();
            }
        }
    }

    /***
     * Interface must be implemented by a view
     * that handles the result of the service responsible for
     * checking the validity of the login.
     */
    public interface IOnLoginActionComplete {
        /**
         * Callback to the event triggered when the login succeed
         */
        public void onLoginSuccess ();
        /**
         * Callback to the event triggered when the login fails
         */
        public void onLoginFailure ();
    }
}
