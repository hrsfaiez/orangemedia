package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.user.domain.service.IUserService;
import tn.dev.orange.orangemedia.user.domain.service.UserService;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

public class UserPictureFragment extends Fragment {

    TextView nameTextView;

    public UserPictureFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.main_fragment_userpicture, container, false);
        nameTextView = (TextView) view.findViewById(R.id.userPicture_name_textView);
        IUserService userModelProvider = UserService.createUserModelProvider();
        User user = userModelProvider.getCurrentUser();
        nameTextView.setText(user.getName());
        return view;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Creates a user fluid picture fragment
     *
     * @return user fluid picture fragment instance
     */
    public static UserPictureFragment newInstance() {
        UserPictureFragment fragment = new UserPictureFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
}
