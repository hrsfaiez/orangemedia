package tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolder;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment.IDeleteRecoveryRequestController;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequestState;

public class RecoveryRequestItemViewHolder
        implements IRecoveryRequestViewHolder<RecoveryRequest, IDeleteRecoveryRequestController> {

    private TextView requestTitleTextView;
    private ImageView requestStateImageView;
    private TextView requestDateTextView;
    private TextView requestMediaTextView;

    private RecoveryRequest recoveryRequest;

    @Override
    public void updateView(View view) {
        requestTitleTextView
                = (TextView)view.findViewById(R.id.recoveryRequestsList_requestTitle_textView);
        requestStateImageView
                = (ImageView)view.findViewById(R.id.recoveryRequestsList_requestState_imageButton);
        requestDateTextView
                = (TextView)view.findViewById(R.id.recoveryRequestsList_requestDate_textView);
        requestMediaTextView
                = (TextView)view.findViewById(R.id.recoveryRequestsList_associatedMedia_textView);
    }

    @Override
    public void setUpModel(RecoveryRequest recoveryRequest) {
        this.recoveryRequest = recoveryRequest;
        requestTitleTextView.setText(recoveryRequest.getTitle());

        Integer stateBitmapId = R.drawable.unseenrequesticon;
        if (recoveryRequest.getState().equals(RecoveryRequestState.seen_responded)) {
            stateBitmapId = R.drawable.respondedrequesticon;
        } else if (recoveryRequest.getState().equals(RecoveryRequestState.seen_notResponded)) {
            stateBitmapId = R.drawable.ignoredrequesticon;
        }
        requestStateImageView.setImageResource(stateBitmapId);

        requestDateTextView.setText(recoveryRequest.getDate().toString().substring(0, 8));
        requestMediaTextView.setText(recoveryRequest.getMedia().toString());
    }

    @Override
    public void attachController(final IDeleteRecoveryRequestController deleteRecoveryRequestController) {
        /*requestDeleteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteRecoveryRequestController.deleteRecoveryRequest(recoveryRequest);
            }
        });*/
    }
}
