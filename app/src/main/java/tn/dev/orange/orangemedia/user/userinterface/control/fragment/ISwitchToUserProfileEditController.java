package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

//TODO:change the name

/**
 * This interface must be implemented by the controller
 * responsible for enabling the form that the user
 * uses to edit his data model.
 */
public interface ISwitchToUserProfileEditController {
    /**
     * Enables the view layout containing the form
     * to edit the user data model.
     */
    public void switchToUserProfileEdit();
}
