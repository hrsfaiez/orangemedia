package tn.dev.orange.orangemedia.user.dataaccess.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by BSI on 12/06/2015.
 */
public class UsersBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "orangeMedia.db";

    private static final String TABLE_REPORTEURS = "table_reporteurs";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_LOGIN = "login";
    private static final int NUM_COL_LOGIN = 1;
    private static final String COL_PASSWORD = "password";
    private static final int NUM_COL_PASSWORD = 2;
    private static final String COL_PHOTO = "photo";
    private static final int NUM_COL_PHOTO = 3;
    private static final String COL_NOM = "nom";
    private static final int NUM_COL_NOM =4;
    private static final String COL_PRENOM = "prenom";
    private static final int NUM_COL_PRENOM = 5;
    private static final String COL_EMAIL = "email";
    private static final int NUM_COL_EMAIL =6;
    private static final String COL_LOCALISATION = "localisation";
    private static final int NUM_COL_LOCALISATION=7;


    private SQLiteDatabase bdd;

    private UserSQLite usereSQLite;

    public UsersBDD(Context context){
        //On crée la BDD et sa table
        usereSQLite = new UserSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = usereSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertLivre(User user){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_LOGIN, user.getLogin());
        values.put(COL_PASSWORD, user.getPassword());
        values.put(COL_PHOTO,user.getPhoto());
        values.put(COL_PRENOM,user.getPrenom());
        values.put(COL_NOM,user.getNom());
        values.put(COL_EMAIL,user.getEmail());
        values.put(COL_LOCALISATION,user.getlocalisation());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_REPORTEURS, null, values);
    }

    public int updateLivre(int id, User user){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_LOGIN, user.getLogin());
        values.put(COL_PASSWORD, user.getPassword());
        values.put(COL_PHOTO,user.getPhoto());
        values.put(COL_PRENOM,user.getPrenom());
        values.put(COL_NOM,user.getNom());
        values.put(COL_EMAIL,user.getEmail());
        values.put(COL_LOCALISATION,user.getlocalisation());
        return bdd.update(TABLE_REPORTEURS, values, COL_ID + " = " +id, null);
    }

    public int removeLivreWithID(int id){
        //Suppression d'un livre de la BDD grâce à l'ID
        return bdd.delete(TABLE_REPORTEURS, COL_ID + " = " +id, null);
    }

    public User getReporteurWithLoginAndPaswword(String login, String password){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_REPORTEURS, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD,COL_PHOTO, COL_PRENOM, COL_NOM, COL_EMAIL, COL_LOCALISATION}, COL_LOGIN + " LIKE \"" + login +"\"" +" AND "+ COL_PASSWORD +" LIKE \"" + password +"\"", null, null, null, null);
        return cursorToLivre(c);
    }
    public User getReporteurWithNomAndPrenom(String nom, String prenom){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_REPORTEURS, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD,COL_PHOTO, COL_PRENOM, COL_NOM, COL_EMAIL, COL_LOCALISATION}, COL_NOM+ " LIKE \"" + nom +"\"" +" AND "+ COL_PRENOM +" LIKE \"" + prenom +"\"", null, null, null, null);
        return cursorToLivre(c);
    }

    public User getReporteurWithEmail(String email){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_REPORTEURS, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD,COL_PHOTO, COL_PRENOM, COL_NOM, COL_EMAIL, COL_LOCALISATION}, COL_EMAIL + " LIKE \"" + email +"\"", null, null, null, null);
        return cursorToLivre(c);
    }

    public User getReporteurWithPhoto(String photo){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_REPORTEURS, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD,COL_PHOTO, COL_PRENOM, COL_NOM, COL_EMAIL, COL_LOCALISATION}, COL_PHOTO+ " LIKE \"" + photo +"\"", null, null, null, null);
        return cursorToLivre(c);
    }

    public User getReporteurWithLocalisation(String localisation){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_REPORTEURS, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD,COL_PHOTO, COL_PRENOM, COL_NOM, COL_EMAIL, COL_LOCALISATION}, COL_LOCALISATION+ " LIKE \"" + localisation +"\"" , null, null, null, null);
        return cursorToLivre(c);
    }
    //Cette méthode permet de convertir un cursor en un livre
    private User cursorToLivre(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un livre
        User user = new User();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        user.setId(c.getInt(NUM_COL_ID));
        user.setLogin(c.getString(NUM_COL_LOGIN));
        user.setPassword(c.getString(NUM_COL_PASSWORD));
        user.setPhoto(c.getString(NUM_COL_PHOTO));
        user.setPrenom(c.getString(NUM_COL_PRENOM));
        user.setNom(c.getString(NUM_COL_NOM));
        user.setEmail(c.getString(NUM_COL_EMAIL));
        user.setlocalisation(c.getString(NUM_COL_LOCALISATION));
        //On ferme le cursor
        c.close();

        //On retourne le livre
        return user;
    }
}
