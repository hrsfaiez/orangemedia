package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control;

import tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder.RecoveryRequestItemViewHolder;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder.RecoveryRequestViewHolder;

public interface IRecoveryRequestViewHolderFactory {
    RecoveryRequestItemViewHolder createRecoveryRequestItemViewHolder();
    RecoveryRequestViewHolder createRecoveryRequestViewHolder();
}
