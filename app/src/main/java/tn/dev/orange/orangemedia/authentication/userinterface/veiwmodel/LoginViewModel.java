package tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Login parcelable model
 */
public class LoginViewModel implements Parcelable{
    private String username;
    public String password;

    public LoginViewModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public LoginViewModel(Parcel in) {
        setUsername(in.readString());
        setPassword(in.readString());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getUsername());
        destination.writeString(getPassword());
    }

    public static final Parcelable.Creator<LoginViewModel> CREATOR
            = new Parcelable.Creator<LoginViewModel>() {
        public LoginViewModel createFromParcel(Parcel in) {
            return new LoginViewModel(in);
        }

        public LoginViewModel[] newArray(int size) {
            return new LoginViewModel[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LoginViewModel))
            return false;
        if (getUsername().equals(((LoginViewModel) o).getUsername())
                && getPassword().equals(((LoginViewModel) o).getPassword()))
            return true;
        else return false;
    }
}
