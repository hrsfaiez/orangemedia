package tn.dev.orange.orangemedia.media.dataaccess.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BSI on 14/06/2015.
 */
public class MediaSQLite extends SQLiteOpenHelper {
    private static final String TABLE_MEDIA = "institutions";
    private static final String COL_ID = "id";
    private static final String COL_LOGIN = "login";
    private static final String COL_PASSWORD = "password";
    private static final String COL_NAME = "nom";
    private static final String COL_EMAIL = "email";
    private static final String COL_TYPE = "type";
    private static final String COL_SIEGE = "siege";

    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_MEDIA + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_LOGIN + " TEXT NOT NULL, "
            + COL_PASSWORD + " TEXT NOT NULL, " + COL_NAME  + " TEXT NOT NULL, "
            + COL_EMAIL  + " TEXT NOT NULL, " + COL_TYPE  + " TEXT NOT NULL, "
            + COL_SIEGE+" TEXT NOT NULL);";



    public MediaSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut faire ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
        //comme ça lorsque je change la version les id repartent de 0
        db.execSQL("DROP TABLE " + TABLE_MEDIA + ";");
        onCreate(db);
    }
}
