package tn.dev.orange.orangemedia.appmodel.viewstate;

public enum MainActivityViewState {
    IDLE,
    USER_STATUS_VIEW_STYLE,
    USER_PICTURE_VIEW_STYLE
}
