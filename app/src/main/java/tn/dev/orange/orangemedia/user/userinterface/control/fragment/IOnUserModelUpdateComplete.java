package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

/**
 * This interface must be implemented by the class
 * that handles the result of user profile update.
 */
public interface IOnUserModelUpdateComplete {
    /**
     * Callback to the event triggered when the service succeeds.
     */
    void onUserModelUpdateSuccess();

    /**
     * Callback to the event triggered when the service fails.
     */
    void onUserModelUpdateFailure();
}
