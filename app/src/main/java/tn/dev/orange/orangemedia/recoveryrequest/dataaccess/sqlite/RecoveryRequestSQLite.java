package tn.dev.orange.orangemedia.recoveryrequest.dataaccess.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BSI on 13/06/2015.
 */
public class RecoveryRequestSQLite  extends SQLiteOpenHelper {


        private static final String TABLE_DEMANDES = "demandes";
        private static final String COL_ID = "ID";
        private static final String COL_CATEGORY = "categorie";
        private static final String COL_LOCALISATION = "localisation";
        private static final String COL_DATE_DEBUT = "dateDebut";
        private static final String COL_DATE_FIN = "dateFin";
        private static final String COL_ID_INSTITUTION_MEDIATIQUE = "id_InstitutionMediatique";


        private static final String CREATE_BDD = "CREATE TABLE " + TABLE_DEMANDES + " ("
                + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_CATEGORY + " TEXT NOT NULL, "
                + COL_LOCALISATION  + " TEXT NOT NULL, " + COL_DATE_DEBUT  + " TEXT NOT NULL, "+
                COL_DATE_FIN+   " TEXT NOT NULL, "+ COL_ID_INSTITUTION_MEDIATIQUE+ " TEXT NOT NULL);";



        public RecoveryRequestSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
            db.execSQL(CREATE_BDD);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //On peut faire ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
            //comme ça lorsque je change la version les id repartent de 0
            db.execSQL("DROP TABLE " + TABLE_DEMANDES + ";");
            onCreate(db);
        }
}
