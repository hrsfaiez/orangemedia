package tn.dev.orange.orangemedia.authentication.userinterface.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel.RecoveringPasswordViewModel;
import tn.dev.orange.orangemedia.authentication.services.RecoveringPasswordService;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachingListener;
import tn.dev.orange.orangemedia.authentication.services.receivers.RecoveringPasswordResultReceiver;

//TODO:save fields values
//TODO:restore fields values
public class RecoveringPasswordFragment extends Fragment
        implements RecoveringPasswordResultReceiver.IOnRecoverPasswordActionComplete {

    private IFragmentAttachingListener mListener;

    private EditText emailEditText;
    private Button submitButton;

    public RecoveringPasswordFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_fragment_recoverpassword, container, false);
        emailEditText = (EditText) view.findViewById
                (R.id.intro_recoverPassword_email_editText);
        submitButton = (Button) view.findViewById(R.id.intro_recoverPassword_submit_button);
        attachListenersToViews();
        return view;
    }

    /**
    * Attaches action listeners to views
    */
    private void attachListenersToViews() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString();
                Parcelable recoverPasswordParcelable = createRecoverPasswordModel(email);
                recoverPassword(recoverPasswordParcelable);
            }
        });
    }

    /**
     * Launches a service to recover the password of the user
     * <p>
     * the result of the password recovery are not handled here
     * </p>
     *
     * @param recoverPasswordParcelable a recovering password model
     * @see RecoveringPasswordResultReceiver.IOnRecoverPasswordActionComplete
     */
    public void recoverPassword (Parcelable recoverPasswordParcelable) {
        Parcelable resultReceiver = createIntroRecoverPasswordResultReceiver();

        Context context = getActivity();
        Intent intent = new Intent(context, RecoveringPasswordService.class);
        intent.setAction(RecoveringPasswordService.ACTION_RECOVER_PASSWORD);
        intent.putExtra(RecoveringPasswordService.EXTRA_RECOVER_PASSWORD_MODEL,
                recoverPasswordParcelable);
        intent.putExtra(RecoveringPasswordService.EXTRA_RECOVER_PASSWORD_RESULT_RECEIVER,
                resultReceiver);
        context.startService(intent);

        //TODO:disabling the recovering button
    }

    /**
     * Creates the result receiver to handle recovering password service result
     *
     * @return a recovering password result receiver
     */
    private RecoveringPasswordResultReceiver createIntroRecoverPasswordResultReceiver() {
        return new RecoveringPasswordResultReceiver(new Handler(), this);
    }

    /**
     * Creates a parcelable recovering password model
     *
     * @param email the email of the user
     * @return a recovering password model
     */
    private RecoveringPasswordViewModel createRecoverPasswordModel(String email) {
        return new RecoveringPasswordViewModel(email);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachingListener) activity);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentAttachingListener");
        }
    }

    @Override
    public void onRecoverPasswordSuccess() {
        mListener.getFragmentMediator().onRecoverPasswordSuccess();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //TODO:behave politely with the service
    }

    /**
     * Creates a password recovery fragment instance
     *
     * @return a recovering password fragment instance
     */
    public static RecoveringPasswordFragment createInstance () {
        return new RecoveringPasswordFragment();
    }

    /***
     * This interface must be implemented by the mediator
     * of the activity that contain this fragment to
     * allow in interaction with this fragment to be communicated
     * to the activity and potentially other fragments contained in it.
     */
    public interface IRecoveringPasswordFragmentInteractionListener {
        /**
         * Callback to the event triggered when the password recovery notification
         * was sent successfully
         */
        public void onRecoverPasswordSuccess();
    }

}
