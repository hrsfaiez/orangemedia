package tn.dev.orange.orangemedia.appmodel.viewstate;


import android.app.FragmentTransaction;
import android.graphics.Point;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.IViewStyleHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.UserPictureFragment;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.UserStatusFragment;

public class ViewStyleHolder implements IViewStyleHolder {
    /**
     * Home activity view state .
     */
    private MainActivityViewState viewState;

    private WindowManager windowManager;
    private LinearLayout topFragmentContainer;
    private LinearLayout contentFragmentContainer;

    /**
     * Helpers app fragments .
     */
    public final UserStatusFragment userStatusFragment = UserStatusFragment.newInstance();
    public final UserPictureFragment userPictureFragment = UserPictureFragment.newInstance();

    public ViewStyleHolder(WindowManager windowManager,
               LinearLayout topFragmentContainer, LinearLayout contentFragmentContainer) {
        this.windowManager = windowManager;
        this.topFragmentContainer = topFragmentContainer;
        this.contentFragmentContainer = contentFragmentContainer;
        viewState = MainActivityViewState.IDLE;
    }

    public boolean isCurrentViewStyle(MainActivityViewState requiredViewStyle) {
        return viewState == requiredViewStyle;
    }


    @Override
    public void switchToViewStyle(MainActivityViewState requiredViewStyle,
                                  FragmentTransaction currentFragmentTransaction) {
        if (MainActivityViewState.USER_STATUS_VIEW_STYLE == requiredViewStyle) {
            switchToUserStatusViewStyle(currentFragmentTransaction);
        } else if (MainActivityViewState.USER_PICTURE_VIEW_STYLE == requiredViewStyle) {
            switchToUserPictureViewStyle(currentFragmentTransaction);
        }
    }

    private void switchToUserPictureViewStyle(FragmentTransaction currentFragmentTransaction) {
        viewState = MainActivityViewState.USER_PICTURE_VIEW_STYLE;
        currentFragmentTransaction.replace(R.id.main_top_fragment_container, userPictureFragment);

        Display display = windowManager.getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        int screenHeight = screenSize.y;
        ViewGroup.LayoutParams topFragmentLayoutParams =
                topFragmentContainer.getLayoutParams();
        topFragmentLayoutParams.height = (int) (screenHeight * 0.55);
        topFragmentContainer.setLayoutParams(topFragmentLayoutParams);

        ViewGroup.LayoutParams contentFragmentLayoutParams =
                contentFragmentContainer.getLayoutParams();
        contentFragmentLayoutParams.height = screenHeight - topFragmentLayoutParams.height;
        contentFragmentContainer.setLayoutParams(contentFragmentLayoutParams);
    }

    private void switchToUserStatusViewStyle(FragmentTransaction currentFragmentTransaction) {
        viewState = MainActivityViewState.USER_STATUS_VIEW_STYLE;

        currentFragmentTransaction.replace(R.id.main_top_fragment_container, userStatusFragment);

        ViewGroup.LayoutParams topFragmentLayoutParams =
                topFragmentContainer.getLayoutParams();
        topFragmentLayoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        topFragmentContainer.setLayoutParams(topFragmentLayoutParams);

        ViewGroup.LayoutParams contentFragmentLayoutParams =
                contentFragmentContainer.getLayoutParams();
        contentFragmentLayoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        contentFragmentContainer.setLayoutParams(contentFragmentLayoutParams);
    }

    public static ViewStyleHolder newInstance (WindowManager windowManager,
                       LinearLayout topFragmentContainer, LinearLayout contentFragmentContainer) {
        return new ViewStyleHolder(windowManager, topFragmentContainer, contentFragmentContainer);
    }
}