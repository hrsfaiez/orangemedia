package tn.dev.orange.orangemedia.authentication.userinterface.control;

/***
 * Interface for transactions between fragments into the intro activity
 *
 */
public interface IFragmentTransactor {
    /**
     * switching from the current fragment to the home fragment
     */
    void switchToHome ();

    /**
     * switching from the current fragment to the login fragment
     */
    void switchToLogin ();

    /**
     * switching from the current fragment to the registration fragment
     */
    void switchToRegister ();

    /**
     * switching from the current fragment to the password recovering fragment
     */
    void switchToRecoverPassword ();
}
