package tn.dev.orange.orangemedia.media.userinterface.viewholder;


import android.view.View;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.media.userinterface.control.IMediaViewHolder;
import tn.dev.orange.orangemedia.media.domain.datamodel.Media;

public class MediaItemViewHolder
        implements IMediaViewHolder<Media> {

    private TextView accreditationDate;
    private TextView mediaName;

    @Override
    public void updateView(View view) {
        accreditationDate
                = (TextView)view.findViewById(R.id.medialist_accriditiondate_textview);
        mediaName = (TextView)view.findViewById(R.id.medialist_medianame_textview);
    }

    @Override
    public void setUpModel(Media media) {
        mediaName.setText(media.getName());
        accreditationDate.setText(media.getAccreditDate().toString().substring(0, 8));
    }
}
