package tn.dev.orange.orangemedia.authentication.userinterface.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel.RegistrationViewModel;
import tn.dev.orange.orangemedia.authentication.services.RegistrationService;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachingListener;
import tn.dev.orange.orangemedia.authentication.services.receivers.RegistrationResultReceiver;

//TODO:save fields values
//TODO:restore fields values
public class RegistrationFragment extends Fragment
    implements RegistrationResultReceiver.IOnRegistrationActionComplete {

    private IFragmentAttachingListener mListener;

    private EditText nomEditText;
    private EditText prenomEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText emailEditText;
    private EditText adressEditText;
    private Button submitButton;

    public RegistrationFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_fragment_register, container, false);
        nomEditText = (EditText) view.findViewById(R.id.intro_register_firstName_editText);
        prenomEditText = (EditText) view.findViewById(R.id.intro_register_lastName_editText);
        usernameEditText = (EditText) view.findViewById(R.id.intro_register_username_editText);
        passwordEditText = (EditText) view.findViewById(R.id.intro_register_password_editText);
        emailEditText = (EditText) view.findViewById(R.id.intro_inscription_email_editText);
        adressEditText = (EditText) view.findViewById(R.id.intro_register_address_editText);
        submitButton = (Button) view.findViewById(R.id.intro_register_submit_button);
        attachListenersToViews();
        return view;
    }

    /*
     * Attaches action listeners to views
     */
    private void attachListenersToViews() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nom = nomEditText.getText().toString();
                String prenom = prenomEditText.getText().toString();
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                String email = emailEditText.getText().toString();
                String adress = adressEditText.getText().toString();
                Parcelable registrationParcelable = createRegistrationModel (nom, prenom, username,
                        password, email, adress);
                register (registrationParcelable);
            }
        });
    }

    /**
     * Launches a service to register the user
     * <p>
     * the result of the registration are not handled here
     * </p>
     *
     * @param registrationParcelable a registration model
     * @see RegistrationResultReceiver.IOnRegistrationActionComplete
     */
    public void register (Parcelable registrationParcelable) {
        Parcelable resultReceiver = createIntroRegistrationResultReceiver();

        Context context = getActivity();
        Intent intent = new Intent(context, RegistrationService.class);
        intent.setAction(RegistrationService.ACTION_REGISTRATION);
        intent.putExtra(RegistrationService.EXTRA_REGISTRATION_MODEL, registrationParcelable);
        intent.putExtra(RegistrationService.EXTRA_REGISTRATION_RESULT_RECEIVER, resultReceiver);
        context.startService(intent);
        //TODO:disabling the registration submit button
    }

    /**
     * Creates the result receiver to handle registration service result
     *
     * @return a registration result receiver
     */
    public RegistrationResultReceiver createIntroRegistrationResultReceiver () {
        return new RegistrationResultReceiver(new Handler(), this);
    }

    /**
     * Creates a parcelable registration model
     *
     * @param nom the first name of the user
     * @param prenom the last name of the user
     * @param username the username of the user
     * @param password the password of the user
     * @param email the email of the user
     * @param address the address of the user
     * @return a registration model
     */
    private RegistrationViewModel createRegistrationModel(String nom, String prenom, String username,
                                               String password, String email, String address) {
        return new RegistrationViewModel(nom, prenom, username,
                password, email, address);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachingListener) activity);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentAttachingListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //TODO:behave nicely with the service
    }

    @Override
    public void onRegistrationFailure() {
        //TODO:making the username and the email fields red or toasting for connection out...
    }

    @Override
    public void onRegistrationSuccess() {
        mListener.getFragmentMediator().onRegisterSuccess();
    }

    /**
     * Creates an inscription fragment instance
     *
     * @return an inscription fragment instance
     */
    public static RegistrationFragment createInstance () {
        return new RegistrationFragment();
    }

    /***
     * This interface must be implemented by the mediator
     * of the activity that contain this fragment to
     * allow in interaction with this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface IInscriptionFragmentInteractionListener {
        /**
         * Callback to the event triggered when the user registers successfully
         */
        public void onRegisterSuccess();
    }

}
