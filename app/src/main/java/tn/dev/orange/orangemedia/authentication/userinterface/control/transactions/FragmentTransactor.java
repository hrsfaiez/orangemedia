package tn.dev.orange.orangemedia.authentication.userinterface.control.transactions;

import android.app.FragmentManager;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.authentication.userinterface.control.IFragmentTransactor;
import tn.dev.orange.orangemedia.authentication.userinterface.control.HomeFragment;
import tn.dev.orange.orangemedia.authentication.userinterface.control.LoginFragment;
import tn.dev.orange.orangemedia.authentication.userinterface.control.RecoveringPasswordFragment;
import tn.dev.orange.orangemedia.authentication.userinterface.control.RegistrationFragment;

public class FragmentTransactor implements IFragmentTransactor {
    /**
     * References to the fragment manager
     */
    private FragmentManager fragmentManager;

    public final HomeFragment homeFragment = HomeFragment.createInstance();
    public final LoginFragment loginFragment = LoginFragment.createInstance();
    public final RegistrationFragment registerFragment = RegistrationFragment.createInstance();
    public final RecoveringPasswordFragment recoverPasswordFragment
            = RecoveringPasswordFragment.createInstance();

    public FragmentTransactor(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void switchToHome() {
        fragmentManager.beginTransaction()
            .replace(R.id.intro_bottom_fragment_container, homeFragment)
            .addToBackStack(null)
            .commit();
    }

    @Override
    public void switchToLogin() {
        fragmentManager.beginTransaction()
            .replace(R.id.intro_bottom_fragment_container, loginFragment)
            .addToBackStack(null)
            .commit();
    }

    @Override
    public void switchToRegister() {
        fragmentManager.beginTransaction()
            .replace(R.id.intro_bottom_fragment_container, registerFragment)
            .addToBackStack(null)
            .commit();
    }

    @Override
    public void switchToRecoverPassword() {
        fragmentManager.beginTransaction()
            .replace(R.id.intro_bottom_fragment_container, recoverPasswordFragment)
            .addToBackStack(null)
            .commit();
    }
}