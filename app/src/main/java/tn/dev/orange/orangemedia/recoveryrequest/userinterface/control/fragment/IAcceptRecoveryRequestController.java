package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment;

import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;

public interface IAcceptRecoveryRequestController {
    void acceptRecoveryRequest(RecoveryRequest recoveryRequest);
}
