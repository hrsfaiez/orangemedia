package tn.dev.orange.orangemedia.contribution.dataaccess.sqlite;

import java.sql.Date;

/**
 * Created by BSI on 10/06/2015.
 */
public class Contribution {
    int id;
    String objet;
    String text;
    String image;
    String date;
    String localisation;
    int id_Reporteur;
    int id_Demande;

    public Contribution() {
        

    }

    public Contribution(int id, String objet, String text, String image, String date, String localisation, int id_Reporteur, int id_Demande) {
        this.id = id;
        this.objet = objet;
        this.text = text;
        this.image = image;
        this.date = date;
        this.localisation = localisation;
        this.id_Reporteur = id_Reporteur;
        this.id_Demande = id_Demande;
    }

    public int getId() {
        return id;
    }

    public String getObjet() {
        return objet;
    }

    public String getText() {
        return text;
    }

    public String getImage() {
        return image;
    }

    public String getDate() {
        return date;
    }

    public String getLocalisation() {
        return localisation;
    }

    public int getId_Reporteur() {
        return id_Reporteur;
    }

    public int getId_Demande() {
        return id_Demande;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Contribution{" +
                "id=" + id +
                ", objet='" + objet + '\'' +
                ", text='" + text + '\'' +
                ", image='" + image + '\'' +
                ", date=" + date +
                ", localisation='" + localisation + '\'' +
                ", id_Reporteur=" + id_Reporteur +
                ", id_Demande=" + id_Demande +
                '}';
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public void setId_Reporteur(int id_Reporteur) {
        this.id_Reporteur = id_Reporteur;
    }

    public void setId_Demande(int id_Demande) {
        this.id_Demande = id_Demande;
    }
}
