package tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder;


import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolder;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment.IAcceptRecoveryRequestController;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;

public class RecoveryRequestViewHolder
        implements IRecoveryRequestViewHolder<RecoveryRequest, IAcceptRecoveryRequestController> {

    private LinearLayout acceptRequestLayout;
    private TextView titleTextView;
    private TextView categoryTextView;
    private TextView creationDateTextView;
    private TextView expirationDateTextView;
    private TextView mediaTextView;
    private TextView contentTextView;

    private RecoveryRequest recoveryRequest;

    @Override
    public void updateView(View view) {
        acceptRequestLayout =
                (LinearLayout) view.findViewById(R.id.recoveryrequestview_contribute_textview);
        titleTextView = (TextView) view.findViewById(R.id.text_view_choix_titre);
        categoryTextView = (TextView) view.findViewById(R.id.text_view_choix_categorie);
        creationDateTextView = (TextView) view.findViewById(R.id.text_view_choix_date_de_creation);
        expirationDateTextView = (TextView) view.findViewById(R.id.text_view_choix_date_expiration);
        mediaTextView = (TextView) view.findViewById(R.id.text_view_choix_media);
        contentTextView = (TextView) view.findViewById(R.id.text_view_choix_contenu);
    }

    @Override
    public void setUpModel(RecoveryRequest recoveryRequest) {
        this.recoveryRequest = recoveryRequest;
        titleTextView.setText(recoveryRequest.getTitle());
        categoryTextView.setText(recoveryRequest.getCategory());
        creationDateTextView.setText(recoveryRequest.getDate().toString().substring(0, 8));
        expirationDateTextView.setText(recoveryRequest.getExpirationDate().toString().substring(0, 8));
        mediaTextView.setText(recoveryRequest.getMedia().toString());
        contentTextView.setText(recoveryRequest.getContent());
    }

    @Override
    public void attachController
            (final IAcceptRecoveryRequestController acceptRecoveryRequestController) {
        acceptRequestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptRecoveryRequestController.acceptRecoveryRequest(recoveryRequest);
            }
        });
    }
}
