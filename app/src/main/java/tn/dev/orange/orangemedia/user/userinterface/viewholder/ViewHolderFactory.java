package tn.dev.orange.orangemedia.user.userinterface.viewholder;

import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolderFactory;

public class ViewHolderFactory implements IUserViewHolderFactory {
    @Override
    public UserProfileEditViewHolder createUserProfileEditViewHolder() {
        UserProfileEditViewHolder viewHolder = new UserProfileEditViewHolder();
        return viewHolder;
    }

    @Override
    public UserProfileViewHolder createUserProfileViewHolder() {
        UserProfileViewHolder userProfileViewHolder = new UserProfileViewHolder();
        return userProfileViewHolder;
    }

    @Override
    public UserStatusViewHolder createUserStatusViewHolder() {
        UserStatusViewHolder userStatusViewHolder = new UserStatusViewHolder();
        return userStatusViewHolder;
    }

}
