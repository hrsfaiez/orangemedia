package tn.dev.orange.orangemedia.appmodel.connector;

/***
 * Interface used by a fragment to get the mediator with the activity it belongs to
 */
public interface IFragmentAttachedListener {
    /**
     * Allows a fragment to retrieve the reference to its listener
     *
     * @return the listener object of the fragment
     */
    <T> T getListener();
}
