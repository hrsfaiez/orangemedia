package tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder;


import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolderFactory;

public class ViewHolderFactory implements IRecoveryRequestViewHolderFactory{
    @Override
    public RecoveryRequestItemViewHolder createRecoveryRequestItemViewHolder() {
        RecoveryRequestItemViewHolder recoveryRequestItemViewHolder
                = new RecoveryRequestItemViewHolder();
        return recoveryRequestItemViewHolder;
    }

    @Override
    public RecoveryRequestViewHolder createRecoveryRequestViewHolder() {
        RecoveryRequestViewHolder recoveryRequestViewHolder =new RecoveryRequestViewHolder();
        return recoveryRequestViewHolder;
    }
}
