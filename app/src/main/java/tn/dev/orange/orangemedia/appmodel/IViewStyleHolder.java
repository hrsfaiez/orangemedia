package tn.dev.orange.orangemedia.appmodel;


import android.app.FragmentTransaction;

import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;

public interface IViewStyleHolder {
    boolean isCurrentViewStyle(MainActivityViewState requiredViewStyle);
    void switchToViewStyle(MainActivityViewState requiredViewStyle,
                           FragmentTransaction currentFragmentTransaction);
}
