package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

import tn.dev.orange.orangemedia.user.userinterface.viewdatamodel.UserProfileViewModel;

/**
 * This interface of the controller responsible
 * for updating the user data model.
 */
public interface IUpdateUserModelController {
    /**
     * Updates the user data model.
     * @param userProfileViewModel the new user
     *            model, that is, the result of
     *            the form filled by the  user.
     */
    public void updateUserModel
            (UserProfileViewModel userProfileViewModel);
}
