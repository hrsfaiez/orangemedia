package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.listadapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.faiezdev.helloandro.R;

import java.util.List;

import tn.dev.orange.orangemedia.recoveryrequest.IRecoveryRequestModelProvider;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolder;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolderFactory;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment.IDeleteRecoveryRequestController;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.recoveryrequest.dataaccess.dataprovider.RecoveryRequestModelProvider;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder.ViewHolderFactory;

public class RecoveryRequestsListAdapter extends ArrayAdapter
                implements IDeleteRecoveryRequestController {

    //TODO:inject this
    public final IRecoveryRequestViewHolderFactory viewHolderFactory = new ViewHolderFactory();
    //TODO:inject this
    public final IRecoveryRequestModelProvider recoveryRequestModelProvider
            = RecoveryRequestModelProvider.newInstance();

    private IRecoveryRequestViewHolder viewHolder;
    private RecoveryRequest recoveryRequest;
    private Context context;

    /**
     * Creates a media list adapter
     *
     * @param context the context of the list provider
     * @param recoveryRequestsList a list of recovery request_d model
     * @return the media list adapter
     */
    public RecoveryRequestsListAdapter(Context context, List recoveryRequestsList) {
        super(context, android.R.layout.simple_list_item_1, recoveryRequestsList);
        this.context = context;
    }


    @Override
    public void deleteRecoveryRequest(RecoveryRequest recoveryRequest) {
        recoveryRequestModelProvider.deleteRequest(recoveryRequest);
    }

    /**
     * Gets a list item view
     *
     * @param position the position of the item int the list
     * @param convertView the convert view
     * @param parent the parent view group
     * @return the list item view
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        recoveryRequest = (RecoveryRequest) getItem(position);

        View view;
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            view = mInflater.inflate(R.layout.main_item_requestslist, null);
            viewHolder = viewHolderFactory.createRecoveryRequestItemViewHolder();
            viewHolder.updateView(view);
            viewHolder.attachController(this);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (IRecoveryRequestViewHolder) view.getTag();
        }
        viewHolder.setUpModel(recoveryRequest);
        return view;
    }

    /**
     * Creates a requests recovery list adapter
     *
     * @param recoveryRequestsList a list of recovery request_d models
     * @return the requests recovery list adapter
     */
    public static RecoveryRequestsListAdapter newInstance(Context context,
                                    List<RecoveryRequest> recoveryRequestsList) {
        return new RecoveryRequestsListAdapter(context, recoveryRequestsList);
    }
}
