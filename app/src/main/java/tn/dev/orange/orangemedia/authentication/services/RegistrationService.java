package tn.dev.orange.orangemedia.authentication.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel.RegistrationViewModel;

public class RegistrationService extends IntentService {
    /*
     * Registration action name
     */
    public static final String ACTION_REGISTRATION = "ACTION_REGISTRATION";

    /*
     * Registration action parameters
     */
    public static final String EXTRA_REGISTRATION_MODEL = "EXTRA_REGISTRATION_MODEL";
    public static final String EXTRA_REGISTRATION_RESULT_RECEIVER
            = "EXTRA_REGISTRATION_RESULT_RECEIVER";

    /*
     * Registration result codes
     */
    public static final int REGISTRATION_SUCCESS = 1;
    public static final int REGISTRATION_FAILURE = 0;
    //TODO:checking for connection out failures ?

    public RegistrationService() {
        super("RegistrationCheckerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (null != intent) {
            final String action = intent.getAction();
            if (ACTION_REGISTRATION.equals(action)) {
                final RegistrationViewModel registrationModel = intent.
                        getParcelableExtra(EXTRA_REGISTRATION_MODEL);
                final ResultReceiver resultReceiver = intent.
                        getParcelableExtra(EXTRA_REGISTRATION_RESULT_RECEIVER);
                handleActionRegister(registrationModel, resultReceiver);
            }
        }
    }

    /**
     * Checks the validity of the registration and passes the result back to the listener
     *
     * @param registrationModel a registration model
     * @param resultReceiver a listener to the result of the registration
     * @see android.os.ResultReceiver
     */
    private void handleActionRegister(RegistrationViewModel registrationModel,
                                      ResultReceiver resultReceiver) {
        // TODO: request_d a web service to check the insertion of the user, then call the result receiver callback

        /*
         * just some glue code
         */
        resultReceiver.send(REGISTRATION_SUCCESS, new Bundle());
    }
}
