package tn.dev.orange.orangemedia.appmodel.connector;

import tn.dev.orange.orangemedia.authentication.userinterface.control.FragmentMediator;

/***
 * Interface used by a fragment to get the mediator with the activity it belongs to
 */
public interface IFragmentAttachingListener {
    /**
     * Allows components of the activity to have a reference to the mediator
     * @return the mediator between the activity and its components
     */
    public FragmentMediator getFragmentMediator();
}
