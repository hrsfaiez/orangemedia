package tn.dev.orange.orangemedia.appmodel.navigationmodel;

import android.view.Menu;
import android.view.MenuItem;
import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.IOnActionBarInteraction;

/**
 * State manager of an action bar
 *
 * @<p>
 *     This class track the current active
 *     item in the main  menu and change it safely
 *     with each menu event.
 * </p>
 */
public class ActionBarStateHolder
        implements IActionBarActionStateHolder, IActionBarViewStateHolder {

    //TODO:inject this
    /**
     * The handler for action bar items clicks.
     */
    private IOnActionBarInteraction actionBarInteractionListener;

    /**
     * The current state;
     */
    MenuItem currentState;

    /**
     * State values.
     */
    public MenuItem Idle;
    public MenuItem UserProfile;
    public MenuItem MediaList;
    public MenuItem RecoveryRequestsList;
    public MenuItem AddContribution;

    public ActionBarStateHolder(Menu menu, IOnActionBarInteraction actionBarInteractionListener) {
        this.actionBarInteractionListener = actionBarInteractionListener;
        updateMenu(menu);
    }

    @Override
    public void updateMenu(Menu menu) {
        Idle = null;
        UserProfile = menu.findItem(R.id.action_viewprofile);
        MediaList = menu.findItem(R.id.action_medialist);
        RecoveryRequestsList = menu.findItem(R.id.action_requestslist);
        AddContribution = menu.findItem(R.id.action_addcontribution);

        currentState = Idle;
    }

    @Override
    public void updateListener(IOnActionBarInteraction actionBarInteractionListener) {
        this.actionBarInteractionListener = actionBarInteractionListener;
    }

    /**
     * Changes the current action bar selected item.
     * If no state matches to the provided item, or
     * the current state matches with the provided item,
     * it does nothing.
     * @param item the action bar item.
     */
    @Override
    public void switchToNewState(MenuItem item) {
        if (isCurrentState(item))
            return;

        if (haveState())
            disableCurrentState();

        currentState = item;

        if (UserProfile.equals(item)) {
            actionBarInteractionListener.viewUserProfile();
            switchToProfile();
            return;
        }
        if (MediaList.equals(item)) {
            actionBarInteractionListener.viewMediaList();
            switchToMedia();
            return;
        }
        if (RecoveryRequestsList.equals(item)) {
            actionBarInteractionListener.viewRequestsList();
            switchToRecoveryRequest();
            return;
        }
        if (AddContribution.equals(item)) {
            actionBarInteractionListener.contribute();
            switchToContribution();
            return;
        }
    }

    /**
     * Disables the current menu state .
     */
    private void disableCurrentState () {
        if (UserProfile.equals(currentState)) {
            currentState.setIcon(R.drawable.user_d);
            return;
        }
        if (MediaList.equals(currentState)) {
            currentState.setIcon(R.drawable.media_d);
            return;
        }
        if (RecoveryRequestsList.equals(currentState)) {
            currentState.setIcon(R.drawable.request_d);
            return;
        }
        if (AddContribution.equals(currentState)) {
            currentState.setIcon(R.drawable.contribute_d);
            return;
        }
    }

    /**
     * Checks whatever is the provided item matches to the
     * current state of the action bar.
     * @param item the menu item.
     * @return the iprovided tem is the the current selected item.
     */
    public Boolean isCurrentState (MenuItem item) {
        if (! haveState())
            return false;

        boolean isCurrentState = (currentState == item);
        return isCurrentState;
    }

    private boolean haveState() {
        return currentState != null;
    }

    @Override
    public void switchToProfile() {
        currentState.setIcon(R.drawable.user_a);
    }

    @Override
    public void switchToMedia() {
        currentState.setIcon(R.drawable.media_a);
    }

    @Override
    public void switchToRecoveryRequest() {
        currentState.setIcon(R.drawable.request_a);
    }

    @Override
    public void switchToContribution() {
        currentState.setIcon(R.drawable.contribute_a);
    }

    public static ActionBarStateHolder newInstance
            (Menu menu, IOnActionBarInteraction actionBarInteractionListener) {
        return new ActionBarStateHolder(menu,actionBarInteractionListener);
    }

}
