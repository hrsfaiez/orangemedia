package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.user.domain.service.IUserService;
import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolderFactory;
import tn.dev.orange.orangemedia.user.domain.service.UserService;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachedListener;
import tn.dev.orange.orangemedia.user.IOnUserEditProfileInteraction;
import tn.dev.orange.orangemedia.user.userinterface.viewdatamodel.UserProfileViewModel;
import tn.dev.orange.orangemedia.user.userinterface.viewholder.ViewHolderFactory;

public class UserEditProfileFragment
        extends Fragment
        implements IOnUserModelUpdateComplete,
                   IUpdateUserModelController{

    //TODO:inject this
    public final IUserViewHolderFactory viewHolderFactory = new ViewHolderFactory();
    //TODO: inject this
    public final IUserService userService = UserService.createUserModelProvider();

    private IUserViewHolder viewHolder;
    private User currentUser;
    private IOnUserEditProfileInteraction mListener;

    private MainActivityViewState requiredViewState;
    public final MainActivityViewState getRequiredViewStyle() {
        return requiredViewState;
    }


    @Override
    public void updateUserModel(UserProfileViewModel userProfile) {
        User newUser = userProfile.mapToModel();
        userService.updateCurrentUserModel(newUser, getActivity(), this);
    }

    @Override
    public void onUserModelUpdateSuccess() {
        mListener.onUpdateUserProfileSuccess();
    }

    @Override
    public void onUserModelUpdateFailure() {
        //TODO:making fields red
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        viewHolder = viewHolderFactory.createUserProfileEditViewHolder();
        currentUser = userService.getCurrentUser();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate
                (R.layout.main_fragment_edituserprofile, container, false);

        viewHolder.updateView(view);
        viewHolder.setUpModel(currentUser);
        viewHolder.attachController(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachedListener) activity).getListener();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement IFragmentAttachedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        currentUser = null;
        viewHolder = null;
    }
    /**
     * Factory method.
     * @return a new instance of this fragment.
     */
    public static UserEditProfileFragment newInstance() {
        UserEditProfileFragment fragment = new UserEditProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.requiredViewState = MainActivityViewState.USER_PICTURE_VIEW_STYLE;
        return fragment;
    }
}
