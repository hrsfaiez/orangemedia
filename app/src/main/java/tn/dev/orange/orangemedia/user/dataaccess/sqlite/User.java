package tn.dev.orange.orangemedia.user.dataaccess.sqlite;


import android.os.Parcel;
import android.os.Parcelable;

public final class User implements Parcelable {

    private int id;
    private String login;
    private String password;
    private String photo;
    private String prenom;
    private String nom;
    private String email;
    private String localisation;

    public User() {
        this.id=0;
        this.login="";
        this.password="";
        this.photo="";
        this.prenom="";
        this.nom="";
        this.email="";
        this.localisation="";
    }

    public User(int id, String login, String password,String photo,String nom, String prenom, String email,
                String localisation) {
        this.id=id;
        this.login = login;
        this.password = password;
        this.photo=photo;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.localisation = localisation;

    }

    public User(Parcel in) {
        setId(in.readInt());
        setLogin(in.readString());
        setPassword(in.readString());
        setPhoto(in.readString());
        setPrenom(in.readString());
        setNom(in.readString());
        setEmail(in.readString());
        setlocalisation(in.readString());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }


    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public void setlocalisation(String localisation) {
        this.localisation = localisation;
    }

    public String getlocalisation() {
        return localisation;
    }

    public String getName () {
        return getPrenom() + " " + getNom();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeInt(getId());
        destination.writeString(getLogin());
        destination.writeString(getPassword());
        destination.writeString(getPhoto());
        destination.writeString(getPrenom());
        destination.writeString(getNom());
        destination.writeString(getEmail());
        destination.writeString(getlocalisation());
    }

    public static final Creator<User> CREATOR
            = new Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) {
            return false;
        }
        if ( getId() == ((User)o).getId() &&
                getPrenom().equals(((User) o).getPrenom())
                && getNom().equals(((User) o).getNom())
                && getLogin().equals(((User) o).getLogin())
                && getPassword().equals(((User) o).getPassword())
                && getEmail().equals(((User) o).getEmail())
                && getlocalisation().equals(((User) o).getlocalisation())) {
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        String userString = getPrenom() + "|" + getNom();
        return userString;
    }
}
