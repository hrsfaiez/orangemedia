package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.user.domain.service.IUserService;
import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolderFactory;
import tn.dev.orange.orangemedia.user.domain.service.UserService;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachedListener;
import tn.dev.orange.orangemedia.user.IOnUserProfileInteraction;
import tn.dev.orange.orangemedia.user.userinterface.viewholder.ViewHolderFactory;

/**
 * The user profile fragment.
 *
 * @<p> This fragment displays all the user
 * data model values.
 * </p>
 */
public class UserProfileFragment
        extends Fragment
        implements ISwitchToUserProfileEditController {


    //TODO:inject this
    public final IUserService modelProvider = UserService.createUserModelProvider();
    //TODO:inject this
    public final IUserViewHolderFactory viewHolderFactory = new ViewHolderFactory();

    private IOnUserProfileInteraction mListener;
    private IUserViewHolder viewHolder;
    private User user;

    private MainActivityViewState requiredViewState;

    public final MainActivityViewState getRequiredViewStyle() {
        return requiredViewState;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        viewHolder = viewHolderFactory.createUserProfileViewHolder();
        user = modelProvider.getCurrentUser();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment_userprofile, container, false);
        viewHolder.updateView(view);
        viewHolder.setUpModel(user);
        viewHolder.attachController(this);
        return view;
    }

    @Override
    public void switchToUserProfileEdit() {
        mListener.editUserProfile();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachedListener) activity).getListener();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement IUserProfileInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        viewHolder = null;
        user = null;
    }

    /**
     * Factory method.
     *
     * @return a new instance of this fragment.
     */
    public static UserProfileFragment newInstance() {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.requiredViewState = MainActivityViewState.USER_PICTURE_VIEW_STYLE;
        return fragment;
    }

}
