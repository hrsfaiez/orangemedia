package tn.dev.orange.orangemedia.media.userinterface.control.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.ListFragment;

import java.util.List;


import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.media.dataaccess.dataprovider.MediaModelProvider;
import tn.dev.orange.orangemedia.media.IMediaModelProvider;
import tn.dev.orange.orangemedia.user.domain.service.UserService;
import tn.dev.orange.orangemedia.media.domain.datamodel.Media;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.user.domain.service.IUserService;
import tn.dev.orange.orangemedia.media.userinterface.control.listadapter.MediaListAdapter;

public class MediaListFragment extends ListFragment {

    //TODO:inject this
    IMediaModelProvider modelProvider = MediaModelProvider.newInstance();
    //TODO:inject this
    IUserService userModelProvider = UserService.createUserModelProvider();

    private MainActivityViewState requiredViewState;

    public final MainActivityViewState getRequiredViewStyle() {
        return requiredViewState;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpMedias();
    }

    /**
     * Inserts the user accredited medias in the list
     */
    private void setUpMedias() {
        User currentUser = userModelProvider.getCurrentUser();
        List<Media> mediaList = modelProvider.getAccreditedMedias(currentUser);

        MediaListAdapter mediaListAdapter =
                MediaListAdapter.newInstance(getActivity(), mediaList);
        setListAdapter(mediaListAdapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Creates a media list fragment
     *
     * @return a media list fragment fragment instance
     */
    public static MediaListFragment newInstance() {
        MediaListFragment fragment = new MediaListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.requiredViewState = MainActivityViewState.USER_STATUS_VIEW_STYLE;
        return fragment;
    }

}
