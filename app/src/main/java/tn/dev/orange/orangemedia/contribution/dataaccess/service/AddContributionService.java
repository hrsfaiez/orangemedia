package tn.dev.orange.orangemedia.contribution.dataaccess.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.contribution.userinterface.view.viewmodel.ContributionViewModel;

public class AddContributionService extends IntentService {
    /*
     * Action of the contribution action
     */
    public static final String ACTION_CONTRIBUTE = "ACTION_CONTRIBUTE";

    /*
     * Parameters of the contribution action
     */
    public static final String EXTRA_CONTRIBUTE_MODEL = "EXTRA_CONTRIBUTE_MODEL";
    public static final String EXTRA_CONTRIBUTE_RESULTRECEIVER = "EXTRA_CONTRIBUTE_RESULTRECEIVER";

    /*
     * Result codes of the contribution action
     */
    public static final int CONTRIBUTE_SUCCESS = 1;
    public static final int CONTRIBUTE_FAILURE = 0;
    //TODO:checking for connection out failures ?

    public AddContributionService() {
        super("AddContributionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_CONTRIBUTE.equals(action)) {
                // handles the contribution

                // retrieves the contribution view model
                final ContributionViewModel contributionViewModel
                        = intent.getParcelableExtra(EXTRA_CONTRIBUTE_MODEL);
                // retrieves the service result receiver
                final ResultReceiver resultReceiver = intent.
                        getParcelableExtra(EXTRA_CONTRIBUTE_RESULTRECEIVER);
                // handles the contribution action
                handleActionContribute(contributionViewModel, resultReceiver);
            }
        }
    }

    /**
     * Sends the contribution to the back-end
     *
     * @param contributionViewModel the contribution view model
     * @param resultReceiver        the service result receiver
     */
    private void handleActionContribute(ContributionViewModel contributionViewModel,
                                        ResultReceiver resultReceiver) {
        // TODO: request_d a web service to add the contribution, then call the result receiver callback
        /*
         * just some glue code
         */
        resultReceiver.send(CONTRIBUTE_SUCCESS, new Bundle());
    }

    /**
     * Starts a service action for a user contribution
     *
     * @param context               the context of the action source
     * @param contributionViewModel the contribution view model
     * @param resultReceiver        the service result receiver
     */
    public static void startActionContribute(Context context,
                                             Parcelable contributionViewModel,
                                             ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, AddContributionService.class);
        intent.setAction(ACTION_CONTRIBUTE);
        intent.putExtra(EXTRA_CONTRIBUTE_MODEL, contributionViewModel);
        intent.putExtra(EXTRA_CONTRIBUTE_RESULTRECEIVER, resultReceiver);
        context.startService(intent);
    }
}
