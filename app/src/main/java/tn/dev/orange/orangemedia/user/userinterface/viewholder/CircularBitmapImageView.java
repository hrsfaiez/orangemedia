package tn.dev.orange.orangemedia.user.userinterface.viewholder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class CircularBitmapImageView extends ImageView{

    public final Bitmap.Config bitmapConfig = Bitmap.Config.ARGB_8888;;

    public CircularBitmapImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularBitmapImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (null == drawable)
            return;
        drawRoundBitmap(canvas);

    }

    private void drawRoundBitmap(Canvas canvas) {
        Drawable drawable = getDrawable();

        // calculates needed values
        Bitmap oldBitmap = ((BitmapDrawable) drawable).getBitmap();
        Rect srcRect = new Rect(0, 0, oldBitmap.getWidth(), oldBitmap.getHeight());
        Integer finalBitmapDiameter = Math.min(getWidth(), getHeight());
        Integer circleCenterOffset = finalBitmapDiameter / 2;
        Integer circleRadius = finalBitmapDiameter / 2;
        Rect dstRect = new Rect(0, 0, finalBitmapDiameter, finalBitmapDiameter);

        Paint paint = new Paint();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG);

        // drawing a circle bitmap
        Bitmap finalBitmap = Bitmap.createBitmap(finalBitmapDiameter, finalBitmapDiameter,
                bitmapConfig);
        Canvas finalCanvas = new Canvas(finalBitmap);
        finalCanvas.drawCircle(circleCenterOffset,
                circleCenterOffset, circleRadius, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        finalCanvas.drawBitmap(oldBitmap, srcRect, dstRect, paint);

        // pasting the result to the canvas
        canvas.drawBitmap(finalBitmap, 0, 0, null);
    }
}
