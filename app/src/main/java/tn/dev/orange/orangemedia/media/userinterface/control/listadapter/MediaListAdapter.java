package tn.dev.orange.orangemedia.media.userinterface.control.listadapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.faiezdev.helloandro.R;

import java.util.List;

import tn.dev.orange.orangemedia.media.userinterface.control.IMediaViewHolder;
import tn.dev.orange.orangemedia.media.userinterface.control.IMediaViewHolderFactory;
import tn.dev.orange.orangemedia.media.domain.datamodel.Media;
import tn.dev.orange.orangemedia.media.userinterface.viewholder.ViewHolderFactory;


public class MediaListAdapter extends ArrayAdapter {

    //TODO:inject this
    private IMediaViewHolderFactory viewHolderFactory = new ViewHolderFactory();

    private IMediaViewHolder viewHolder;
    private Media media;
    private Context context;

    public MediaListAdapter(Context context, List objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.context = context;
    }

    /**
     * Gets a list item view
     *
     * @param position the position of the item int the list
     * @param convertView the convert view
     * @param parent the parent view group
     * @return the list item view
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        media = (Media) getItem(position);

        View view;
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (null == convertView) {
            view = mInflater.inflate(R.layout.main_item_medialist, null);
            viewHolder = viewHolderFactory.createMediaItemViewHolder();
            viewHolder.updateView(view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (IMediaViewHolder) view.getTag();
        }
        viewHolder.setUpModel(media);
        return view;
    }



    /**
     * Creates a media list adapter
     *
     * @param context the context of the list provider
     * @param mediaList a list of media model
     * @return the media list adapter
     */
    public static MediaListAdapter newInstance(Context context, List<Media> mediaList) {
        return new MediaListAdapter(context, mediaList);
    }

}
