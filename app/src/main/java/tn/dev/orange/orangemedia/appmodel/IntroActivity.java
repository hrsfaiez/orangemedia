package tn.dev.orange.orangemedia.appmodel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachingListener;
import tn.dev.orange.orangemedia.authentication.userinterface.control.FragmentMediator;
import tn.dev.orange.orangemedia.authentication.userinterface.control.IFragmentTransactor;
import tn.dev.orange.orangemedia.authentication.userinterface.control.transactions.FragmentTransactor;

public class IntroActivity extends Activity
        implements IFragmentAttachingListener,
        FragmentMediator.IActivityMainSwitchListener {

    /**
     * Element responsible for translations between fragments
     */
    private IFragmentTransactor introFragmentTransactor;

    /**
     * Element responsible for mediating between the activity and its fragments
     *
     * <p>
     * it is a listener for fragments' events that lead to a fragment update
     * </p>
     */
    private FragmentMediator introFragmentMediator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_activity);
        getFragmentTransactor().switchToHome();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //TODO:save the content fragment
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_intro, menu);
        Drawable actionBarBackground = getResources().getDrawable(R.drawable.transparentbackground);
        getActionBar().setBackgroundDrawable(actionBarBackground);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(false);
        return true;
    }

    /**
     * Gets the fragment mediator
     *
     * this getter uses the lazy loading to create a FragmentMediator object
     *
     * @return the fragment mediator attribute object
     * @see #createIntroFragmentMediator
     */
    @Override
    public FragmentMediator getFragmentMediator() {
        if (introFragmentMediator == null) {
            introFragmentTransactor = getFragmentTransactor();
            introFragmentMediator
                    = createIntroFragmentMediator(introFragmentTransactor, this);
        }
        return introFragmentMediator;
    }

    /**
     * Gets the fragment transactor
     *
     * this getter uses lazy loading to create a FragmentTranslator object
     *
     * @return the fragment transactor attribute object
     * @see #createIntroFragmentTransactor
     */

    private IFragmentTransactor getFragmentTransactor () {
        if (introFragmentTransactor == null) {
            introFragmentTransactor = createIntroFragmentTransactor();
        }
        return introFragmentTransactor;
    }

    /**
     * Creates a fragment transactor
     *
     * @precondition the current activity must be already created
     * @return a fragment transactor instance
     */
    public IFragmentTransactor createIntroFragmentTransactor () {
        return new FragmentTransactor(this.getFragmentManager());
    }

    /**
     * Creates a fragment mediator
     *
     * @return a fragment mediator instance
     */
    public FragmentMediator createIntroFragmentMediator
                (IFragmentTransactor introFragmentTranslator,
                 FragmentMediator.IActivityMainSwitchListener activityMainSwitchListener) {
        return new FragmentMediator(introFragmentTranslator, activityMainSwitchListener);
    }

    /**
     * Callback to the event triggered when a switch to the main activity is needed
     *
     * @param mainActivity
     */
    @Override
    public void switchToHomeActivity(Class<HomeActivity> mainActivity) {
        Context applicationContext = getApplicationContext();
        Intent mainActivitySwitchIntent = new Intent(applicationContext, mainActivity);
        mainActivitySwitchIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(mainActivitySwitchIntent);
    }
}
