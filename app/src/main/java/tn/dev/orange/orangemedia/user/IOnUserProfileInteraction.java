package tn.dev.orange.orangemedia.user;

public interface IOnUserProfileInteraction {
    void editUserProfile();
}
