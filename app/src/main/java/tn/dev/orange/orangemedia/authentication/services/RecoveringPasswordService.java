package tn.dev.orange.orangemedia.authentication.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel.RecoveringPasswordViewModel;


public class RecoveringPasswordService extends IntentService {
    /*
     * Recovering password action name
     */
    public static final String ACTION_RECOVER_PASSWORD = "ACTION_RECOVER_PASSWORD";

    /*
     * Recovering password action parameters
     */
    public static final String EXTRA_RECOVER_PASSWORD_MODEL = "EXTRA_RECOVER_PASSWORD_MODEL";
    public static final String EXTRA_RECOVER_PASSWORD_RESULT_RECEIVER
            = "EXTRA_RECOVER_PASSWORD_RESULT_RECEIVER";

    /*
     * Recovering password result codes
     */
    public static final int RECOVER_PASSWORD_SUCCESS = 1;
    //TODO:checking for connection out failures ?

    public RecoveringPasswordService() {
        super("RecoverPasswordCheckerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (null != intent) {
            final String action = intent.getAction();
            if (ACTION_RECOVER_PASSWORD.equals(action)) {
                final RecoveringPasswordViewModel recoveringPasswordModel =
                        intent.getParcelableExtra(EXTRA_RECOVER_PASSWORD_MODEL);
                final ResultReceiver resultReceiver = intent.
                        getParcelableExtra(EXTRA_RECOVER_PASSWORD_RESULT_RECEIVER);
                handleActionLogin(recoveringPasswordModel, resultReceiver);
            }
        }
    }

    /**
     * Sends a recovering password request_d and passes the result back to the listener
     *
     * @param recoveringPasswordModel a recovering password method
     * @param resultReceiver a listener to the result of the password recovery
     * @see android.os.ResultReceiver
     */
    private void handleActionLogin(RecoveringPasswordViewModel recoveringPasswordModel,
                                   ResultReceiver resultReceiver) {
        // TODO: request_d a web service to recover user password
        resultReceiver.send(RECOVER_PASSWORD_SUCCESS, new Bundle());
    }

}
