package tn.dev.orange.orangemedia.media.dataaccess.sqlite;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * a media model
 */
public class Media implements Parcelable {
    private  int id;
    private String login;
    private String password;
    private String name;
    private String email;
    private  String type;
    private String siege;

    public Media() {
        this.id = 0;
        this.login = "";
        this.password ="";
        this.name = "";
        this.email = "";
        this.type = "";
        this.siege ="";
    }

    public Media(int id, String login,String password , String name, String email,
    String type, String siege) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.email = email;
        this.type= type;
        this.siege = siege;

    }

    public Media(Parcel in) {
        setId(in.readInt());
        setLogin(in.readString());
        setPassword(in.readString());
        setName(in.readString());
        setEmail(in.readString());
        setType(in.readString());
        setSiege(in.readString());


    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }





    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeInt(getId());
        destination.writeString(getPassword());
        destination.writeString(getName());
        destination.writeString(getEmail());
        destination.writeString(getType());
        destination.writeString(getSiege());

    }

    public static final Creator<Media> CREATOR
            = new Creator<Media>() {
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }


        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Media)) {
            return false;
        }
        if (    getId() == ((Media) o).getId()
                && getName().equals(((Media) o).getName())
                && getLogin().equals(((Media) o).getLogin())
                && getPassword().equals(((Media) o).getPassword())
                && getEmail().equals(((Media) o).getEmail())
                && getType().equals(((Media) o).getType())
                && getSiege().equals(((Media) o).getSiege())) {
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        String mediaString = getName();
        return mediaString;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public String getSiege() {
        return siege;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSiege(String siege) {
        this.siege = siege;
    }
}
