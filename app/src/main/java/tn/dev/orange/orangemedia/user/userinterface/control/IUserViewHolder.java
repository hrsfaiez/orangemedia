package tn.dev.orange.orangemedia.user.userinterface.control;


import android.view.View;

public interface IUserViewHolder<TModel, TController> {
    void updateView (View view);
    void setUpModel(TModel model);
    void attachController(final TController controller);
}
