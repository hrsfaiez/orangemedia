package tn.dev.orange.orangemedia.appmodel;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.connector.FragmentMediator;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachedListener;
import tn.dev.orange.orangemedia.appmodel.viewstate.ViewStyleHolder;
import tn.dev.orange.orangemedia.appmodel.navigationmodel.ActionBarStateHolder;
import tn.dev.orange.orangemedia.appmodel.navigationmodel.IActionBarActionStateHolder;
import tn.dev.orange.orangemedia.appmodel.navigationmodel.IActionBarViewStateHolder;

public class HomeActivity extends Activity
        implements IFragmentAttachedListener {

    /**
     * Mediator that intermediates between the activity and its sub-components
     */
    private FragmentMediator fragmentMediator;

    /**
     * Manages the action bar state .
     */
    private IActionBarViewStateHolder actionBarViewStateHolder;
    private IActionBarActionStateHolder actionBarActionStateHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        LinearLayout topFragmentContainer =
                (LinearLayout) findViewById(R.id.main_top_fragment_container_layout);
        LinearLayout contentFragmentContainer =
                (LinearLayout) findViewById(R.id.main_middle_fragment_container_layout);
        // creating a fragment mediator
        ViewStyleHolder viewStyleHolder = ViewStyleHolder.newInstance(getWindowManager(),
                topFragmentContainer, contentFragmentContainer);
        FragmentMediator fragmentMediator
                = FragmentMediator.newInstance(getFragmentManager(), viewStyleHolder);
        // updating the activity mediator
        this.fragmentMediator = fragmentMediator;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        IOnActionBarInteraction actionBarInteractionListener =
                getListener();
        ActionBarStateHolder actionBarStateHolder =
                ActionBarStateHolder.newInstance(menu, actionBarInteractionListener);

        actionBarActionStateHolder = actionBarStateHolder;
        actionBarViewStateHolder = actionBarStateHolder;

        Drawable actionBarBackground = getResources().getDrawable(R.drawable.actionbarbackground);
        getActionBar().setBackgroundDrawable(actionBarBackground);
        getActionBar().setDisplayUseLogoEnabled(false);
        getActionBar().setDisplayShowHomeEnabled(false);

        MenuItem defautMenuItem = menu.findItem(R.id.action_viewprofile);
        actionBarActionStateHolder.switchToNewState(defautMenuItem);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        actionBarActionStateHolder.switchToNewState(item);
        return true;
        //return super.onOptionsItemSelected(item);
    }

    @Override
    public <T> T getListener() {
        T fragmentListener = (T) fragmentMediator;
        return fragmentListener;
    }
}
