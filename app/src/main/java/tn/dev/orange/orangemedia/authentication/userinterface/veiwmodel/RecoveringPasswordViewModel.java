package tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Recovering password parcelable model
 */
public class RecoveringPasswordViewModel implements Parcelable {

    private String email;

    public RecoveringPasswordViewModel(String email) {
        this.email = email;
    }

    public RecoveringPasswordViewModel(Parcel in) {
        setEmail(in.readString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getEmail());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static final Parcelable.Creator<RecoveringPasswordViewModel> CREATOR
            = new Parcelable.Creator<RecoveringPasswordViewModel>() {
        public RecoveringPasswordViewModel createFromParcel(Parcel in) {
            return new RecoveringPasswordViewModel(in);
        }

        public RecoveringPasswordViewModel[] newArray(int size) {
            return new RecoveringPasswordViewModel[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RecoveringPasswordViewModel))
            return false;
        if (getEmail().equals(((RecoveringPasswordViewModel) o).getEmail()))
            return true;
        else return false;
    }
}
