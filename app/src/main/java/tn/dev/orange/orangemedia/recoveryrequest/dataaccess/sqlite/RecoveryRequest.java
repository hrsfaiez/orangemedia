package tn.dev.orange.orangemedia.recoveryrequest.dataaccess.sqlite;

import android.os.Parcel;
import android.os.Parcelable;

import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequestState;

/**
 * a recovery request_d model
 */
public final class RecoveryRequest implements Parcelable {
    private int id;
    private String category;
    private String localisation;
    private String dateDebut;
    private String dateFin;
    int id_institutionMediatique;

    public RecoveryRequest() {
        this.id =0;
        this.category="";
        this.localisation="";
        this.dateDebut="";
        this.dateFin="";
        this.id_institutionMediatique=0;

    }

    public RecoveryRequest(int id, String category,String  dateDebut,String localisation,
                          String dateFin, int id_institutionMediatique){
        setId(id);
        setCategory(category);
        setDateDebut(dateDebut);
        setDateFin(dateFin);
        setLocalisation(localisation);
        setId_institutionMediatique(id_institutionMediatique);
    }

    public RecoveryRequest(Parcel in) {

         setId(in.readInt());
         setCategory(in.readString());
        /*Date date = new Date(in.readLong());
        setDate(date);
        Date expirationDate = new Date(in.readLong());
        setExpirationDate(expirationDate);*/

        setDateDebut(in.readString());
        setDateFin(in.readString());
        setLocalisation(in.readString());
        setId_institutionMediatique(in.readInt());
    }

    public RecoveryRequestState extractRecoveryState(Integer recoveryRequestState) {
        if (RecoveryRequestState.seen_responded.getValue().equals(recoveryRequestState)) {
            return RecoveryRequestState.seen_responded;
        } else if (RecoveryRequestState.seen_notResponded.getValue().equals(recoveryRequestState)){
            return RecoveryRequestState.seen_notResponded;
        } else {
            return RecoveryRequestState.notSeen;
        }
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getLocalisation() {
        return localisation;
    }

    public String getDateDebut() {
        return dateDebut;   }

    public String getDateFin() {
        return dateFin;
    }

    public int getId_institutionMediatique() {
        return id_institutionMediatique;
    }

    public void setId_institutionMediatique(int id_institutionMediatique) {
        this.id_institutionMediatique = id_institutionMediatique;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeInt(getId());
        destination.writeString(getCategory());
       /* destination.writeLong(getDate().getTime());
        destination.writeLong(getExpirationDate().getTime());*/
        destination.writeString(getDateDebut());
        destination.writeString(getDateFin());
        destination.writeString(getLocalisation());
        destination.writeInt(getId_institutionMediatique());
    }

    public static final Creator<RecoveryRequest> CREATOR
            = new Creator<RecoveryRequest>() {
        public RecoveryRequest createFromParcel(Parcel in) {
            return new RecoveryRequest(in);
        }


        public RecoveryRequest[] newArray(int size) {
            return new RecoveryRequest[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RecoveryRequest)) {
            return false;
        }
        if (getId()== ((RecoveryRequest) o).getId()
                && getCategory().equals(((RecoveryRequest) o).getCategory())
                /*&& getDate().equals(((RecoveryRequest) o).getDate())
                && getExpirationDate().equals(((RecoveryRequest) o).getExpirationDate())*/
                && getDateDebut().equals(((RecoveryRequest) o).getDateDebut())
                && getDateFin().equals(((RecoveryRequest) o).getDateFin())
                && getLocalisation().equals(((RecoveryRequest) o).getLocalisation())
                && getId_institutionMediatique() == ((RecoveryRequest) o).getId_institutionMediatique()) {
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        return getId() + " "+ getCategory() ;
    }
}
