package tn.dev.orange.orangemedia.appmodel.connector;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.IOnActionBarInteraction;
import tn.dev.orange.orangemedia.appmodel.IViewStyleHolder;
import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.appmodel.viewstate.ViewStyleHolder;
import tn.dev.orange.orangemedia.media.userinterface.control.fragment.MediaListFragment;
import tn.dev.orange.orangemedia.recoveryrequest.IOnRecoveryRequestInteractionListener;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment.IOnRecoveryRequestSelectSelect;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment.RecoveryRequestFragment;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.UserEditProfileFragment;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.UserProfileFragment;
import tn.dev.orange.orangemedia.user.IOnUserEditProfileInteraction;
import tn.dev.orange.orangemedia.user.IOnUserProfileInteraction;
import tn.dev.orange.orangemedia.contribution.userinterface.control.ContributeFragment;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment.RecoveryRequestsListFragment;

public class FragmentMediator
        implements IOnActionBarInteraction,
        IOnRecoveryRequestSelectSelect,
        IOnUserProfileInteraction,
        ContributeFragment.IOnContributionInteraction,
        IOnUserEditProfileInteraction,
        IOnRecoveryRequestInteractionListener {

    private FragmentManager fragmentManager;

    IViewStyleHolder viewStyleHolder;
    /**
     * Main activity fragments .
     */
    public final ContributeFragment contributeFragment = ContributeFragment.newInstance();
    public final RecoveryRequestsListFragment recoveryRequestsListFragment =
            RecoveryRequestsListFragment.newInstance();
    public final UserProfileFragment profileFragment = UserProfileFragment.newInstance();
    public final UserEditProfileFragment userEditProfileFragment =
            UserEditProfileFragment.newInstance();
    public final MediaListFragment mediaListFragment = MediaListFragment.newInstance();
    public final RecoveryRequestFragment recoveryRequestFragment =
            RecoveryRequestFragment.newInstance();

    public FragmentMediator(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void contribute() {
        MainActivityViewState requiredViewStyle = contributeFragment.getRequiredViewStyle();
        resetRecoveryRequestModelToContribute();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle(requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, contributeFragment)
                .commit();
    }

    @Override
    public void respondToRecoveryRequest(RecoveryRequest recoveryRequest) {
        MainActivityViewState requiredViewStyle = contributeFragment.getRequiredViewStyle();
        insertRecoveryRequestModelToContribute(recoveryRequest);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle(requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, contributeFragment)
                .commit();
    }

    @Override
    public void viewMediaList() {
        MainActivityViewState requiredViewStyle = mediaListFragment.getRequiredViewStyle();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle (requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, mediaListFragment)
                .commit();
    }

    @Override
    public void viewRequestsList() {
        MainActivityViewState requiredViewStyle = recoveryRequestsListFragment.getRequiredViewStyle();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle(requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, recoveryRequestsListFragment)
                .commit();
    }
    @Override
    public void viewRecoveryRequest(RecoveryRequest recoveryRequest) {
        MainActivityViewState requiredViewStyle = recoveryRequestFragment.getRequiredViewStyle();
        insertRecoveryRequestModelToView(recoveryRequest);
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle(requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, recoveryRequestFragment)
                .commit();
    }

    @Override
    public void viewUserProfile() {
        MainActivityViewState requiredViewStyle = profileFragment.getRequiredViewStyle();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle(requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, profileFragment)
                .commit();
    }

    @Override
    public void editUserProfile() {
        MainActivityViewState requiredViewStyle = userEditProfileFragment.getRequiredViewStyle();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if (! viewStyleHolder.isCurrentViewStyle(requiredViewStyle)) {
            viewStyleHolder.switchToViewStyle(requiredViewStyle, ft);
        }
        ft.replace(R.id.main_middle_fragment_container, userEditProfileFragment)
                .commit();
    }

    private void insertRecoveryRequestModelToContribute(RecoveryRequest recoveryRequest) {
        if (null == contributeFragment.getArguments()) {
            Bundle bundle = new Bundle();
            contributeFragment.setArguments(bundle);
        } else {
            if (null != contributeFragment.getArguments().
                    get(ContributeFragment.REQUESTRECOVERY_ARG_NAME)) {
                contributeFragment.getArguments().
                        remove(ContributeFragment.REQUESTRECOVERY_ARG_NAME);
            }
        }
        contributeFragment.getArguments().
                putParcelable(ContributeFragment.REQUESTRECOVERY_ARG_NAME,
                        recoveryRequest);
    }

    private void resetRecoveryRequestModelToContribute() {
        insertRecoveryRequestModelToContribute(null);
    }

    private void insertRecoveryRequestModelToView(RecoveryRequest recoveryRequest) {
        if (null == recoveryRequestFragment.getArguments()) {
            Bundle bundle = new Bundle();
            recoveryRequestFragment.setArguments(bundle);
        } else {
            if (null != recoveryRequestFragment.getArguments().
                    get(RecoveryRequestFragment.RECOVERY_REQUEST_ARG_NAME)) {
                recoveryRequestFragment.getArguments().
                        remove(RecoveryRequestFragment.RECOVERY_REQUEST_ARG_NAME);
            }
        }
        recoveryRequestFragment.getArguments().
                putParcelable(RecoveryRequestFragment.RECOVERY_REQUEST_ARG_NAME,
                        recoveryRequest);
    }

    @Override
    public void onUpdateUserProfileSuccess() {

    }

    @Override
    public void onContributeSuccess() {
    }


    /**
     * Creates a fragment mediator
     *
     * @return a fragment mediator instance
     */
    public static FragmentMediator newInstance
    (FragmentManager fragmentManager, ViewStyleHolder viewStyleHolder) {
        FragmentMediator fragmentMediator = new FragmentMediator(fragmentManager);
        fragmentMediator.viewStyleHolder = viewStyleHolder;
        return fragmentMediator;
    }


}