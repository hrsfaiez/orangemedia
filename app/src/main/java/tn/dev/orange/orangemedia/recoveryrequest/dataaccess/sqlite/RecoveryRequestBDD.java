package tn.dev.orange.orangemedia.recoveryrequest.dataaccess.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


/**
 * Created by BSI on 13/06/2015.
 */
public class RecoveryRequestBDD {


    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "orangeMedia.db";

    private static final String TABLE_DEMANDES = "demandes";
    private static final String COL_ID = "id";
    private static final int NUM_COL_ID = 0;
    private static final String COL_CATEGORY = "categorie";
    private static final int NUM_COL_CATEGORY = 1;
    private static final String COL_LOCALISATION = "localisation";
    private static final int NUM_COL_LOCALISATION = 2;
    private static final String COL_DATE_DEBUT = "dateDebut";
    private static final int NUM_COL_DATE_DEBUT = 3;
    private static final String COL_DATE_FIN = "dateFin";
    private static final int NUM_COL_DATE_FIN = 4;
    private static final String COL_ID_INSTITUTION_MEDIATIQUE = "id_InstitutionMediatique";
    private static final int NUM_COL_ID_INSTITUTION_MEDIATIQUE = 5;



    private SQLiteDatabase bdd;

    private RecoveryRequestSQLite recoveryRequestSQLite;

    public RecoveryRequestBDD(Context context){
        //On crée la BDD et sa table
        recoveryRequestSQLite = new RecoveryRequestSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = recoveryRequestSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertRecoveryRequest(RecoveryRequest recoveryRequest){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
       values.put(COL_CATEGORY, recoveryRequest.getCategory());
       values.put(COL_LOCALISATION, recoveryRequest.getLocalisation());
       values.put(COL_DATE_DEBUT, recoveryRequest.getDateDebut());
       values.put(COL_DATE_FIN, recoveryRequest.getDateFin());
        values.put(COL_ID_INSTITUTION_MEDIATIQUE, recoveryRequest.getId_institutionMediatique());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_DEMANDES, null, values);
    }

    public int updateRecoveryRequest(int id, RecoveryRequest recoveryRequest){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_CATEGORY, recoveryRequest.getCategory());
        values.put(COL_LOCALISATION, recoveryRequest.getLocalisation());
        values.put(COL_DATE_DEBUT, recoveryRequest.getDateDebut());
        values.put(COL_DATE_FIN, recoveryRequest.getDateFin());
        values.put(COL_ID_INSTITUTION_MEDIATIQUE, recoveryRequest.getId_institutionMediatique());

        return bdd.update(TABLE_DEMANDES, values, COL_ID + " = " +id, null);
    }

    public int removeRecoveryRequestWithId(int id){
        //Suppression d'une demande de la BDD grâce à l'ID
        return bdd.delete(TABLE_DEMANDES, COL_ID + " = " +id, null);
    }

    public RecoveryRequest getRecoveryRequestWithCategory(String category){
        //Récupère dans un Cursor les valeurs correspondant à un livre contenu dans la BDD (ici on sélectionne la demande grâce à sa categorie)
        Cursor c = bdd.query(TABLE_DEMANDES, new String[] {COL_ID, COL_CATEGORY, COL_LOCALISATION, COL_DATE_DEBUT, COL_DATE_FIN, COL_ID_INSTITUTION_MEDIATIQUE},
                COL_CATEGORY + " LIKE \"" + category +"\"", null, null, null, null);
        return cursorToRecoveryRequest(c);
    }

    public RecoveryRequest getRecoveryRequestWithLocalisation(String localisation){
        //Récupère dans un Cursor les valeurs correspondant à un demande  contenue dans la BDD (ici on sélectionne la demande grâce à sa localisation)
        Cursor c = bdd.query(TABLE_DEMANDES, new String[] {COL_ID, COL_CATEGORY, COL_LOCALISATION, COL_DATE_DEBUT, COL_DATE_FIN, COL_ID_INSTITUTION_MEDIATIQUE},
                COL_LOCALISATION + " LIKE \"" + localisation +"\"", null, null, null, null);
        return cursorToRecoveryRequest(c);
    }

    public RecoveryRequest getRecoveryRequestWithDate_Debut_Fin(String dateDebut, String dateFin){
        //Récupère dans un Cursor les valeurs correspondant à un demande  contenue dans la BDD (ici on sélectionne la demande grâce à sa date de debut et sa date de fin)
        Cursor c = bdd.query(TABLE_DEMANDES, new String[] {COL_ID, COL_CATEGORY, COL_LOCALISATION, COL_DATE_DEBUT, COL_DATE_FIN, COL_ID_INSTITUTION_MEDIATIQUE},
                COL_DATE_DEBUT + " LIKE \"" + dateDebut +"\""+ " AND " + COL_DATE_FIN +   " LIKE \"" + dateFin +"\"", null, null, null, null);
        return cursorToRecoveryRequest(c);
    }


    //Cette méthode permet de convertir un cursor en une demande
    private RecoveryRequest cursorToRecoveryRequest(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un livre
        RecoveryRequest recoveryRequest = new RecoveryRequest();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        recoveryRequest.setId(c.getInt(NUM_COL_ID));
        recoveryRequest.setCategory(c.getString(NUM_COL_CATEGORY));
        recoveryRequest.setLocalisation(c.getString(NUM_COL_LOCALISATION));
        recoveryRequest.setDateDebut(c.getString(NUM_COL_DATE_DEBUT));
        recoveryRequest.setDateFin(c.getString(NUM_COL_DATE_FIN));
        recoveryRequest.setId_institutionMediatique(c.getInt(NUM_COL_ID_INSTITUTION_MEDIATIQUE));
        //On ferme le cursor
        c.close();

        //On retourne lea demande
        return recoveryRequest;
    }

}
