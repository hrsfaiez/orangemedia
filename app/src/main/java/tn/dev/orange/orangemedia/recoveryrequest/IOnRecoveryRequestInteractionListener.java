package tn.dev.orange.orangemedia.recoveryrequest;

import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;

public interface IOnRecoveryRequestInteractionListener {
    void respondToRecoveryRequest(RecoveryRequest recoveryRequest1);
}
