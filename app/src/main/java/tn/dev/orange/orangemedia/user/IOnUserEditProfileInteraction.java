package tn.dev.orange.orangemedia.user;

public interface IOnUserEditProfileInteraction {
    void onUpdateUserProfileSuccess();
}
