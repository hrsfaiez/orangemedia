package tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Registration parcelable model
 */
public class RegistrationViewModel implements Parcelable {

    private String nom;
    private String prenom;
    private String username;
    private String password;
    private String email;
    private String adress;

    public RegistrationViewModel(String nom, String prenom, String username, String password, String email, String address) {
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.email = email;
        this.adress = adress;
    }

    public RegistrationViewModel(Parcel in) {
        setNom(in.readString());
        setPrenom(in.readString());
        setUsername(in.readString());
        setPassword(in.readString());
        setEmail(in.readString());
        setAdress(in.readString());
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public static final Parcelable.Creator<RegistrationViewModel> CREATOR
            = new Parcelable.Creator<RegistrationViewModel>() {
        public RegistrationViewModel createFromParcel(Parcel in) {
            return new RegistrationViewModel(in);
        }

        public RegistrationViewModel[] newArray(int size) {
            return new RegistrationViewModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getNom());
        destination.writeString(getPrenom());
        destination.writeString(getUsername());
        destination.writeString(getPassword());
        destination.writeString(getEmail());
        destination.writeString(getAdress());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RegistrationViewModel))
            return false;
        if (getNom().equals(((RegistrationViewModel) o).getNom())
                && getPrenom().equals(((RegistrationViewModel) o).getPrenom())
                && getUsername().equals(((RegistrationViewModel) o).getUsername())
                && getPassword().equals(((RegistrationViewModel) o).getPassword())
                && getEmail().equals(((RegistrationViewModel) o).getEmail())
                && getAdress().equals(((RegistrationViewModel) o).getAdress()))
            return true;
        else return false;
    }
}
