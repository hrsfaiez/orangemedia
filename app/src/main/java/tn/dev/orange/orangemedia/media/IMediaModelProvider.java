package tn.dev.orange.orangemedia.media;

import java.util.List;

import tn.dev.orange.orangemedia.media.domain.datamodel.Media;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

public interface IMediaModelProvider {
    List<Media> getAccreditedMedias (User user);
}
