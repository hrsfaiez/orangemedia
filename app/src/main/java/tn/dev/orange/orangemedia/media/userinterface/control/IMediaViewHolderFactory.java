package tn.dev.orange.orangemedia.media.userinterface.control;

import tn.dev.orange.orangemedia.media.userinterface.viewholder.MediaItemViewHolder;

public interface IMediaViewHolderFactory {
    MediaItemViewHolder createMediaItemViewHolder();
}
