package tn.dev.orange.orangemedia.media.dataaccess.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


/**
 * Created by BSI on 14/06/2015.
 */
public class MediaBDD {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "orangeMedia.db";

    private static final String TABLE_MEDIA = "institutions";
    private static final String COL_ID = "id";
    private static final int NUM_COL_ID = 0;
    private static final String COL_LOGIN = "login";
    private static final int NUM_COL_LOGIN = 1;
    private static final String COL_PASSWORD = "password";
    private static final int NUM_COL_PASSWORD = 2;
    private static final String COL_NAME = "nom";
    private static final int NUM_COL_NAME = 3;
    private static final String COL_EMAIL = "email";
    private static final int NUM_COL_EMAIL = 4;
    private static final String COL_TYPE = "type";
    private static final int NUM_COL_TYPE = 5;
    private static final String COL_SIEGE = "siege";
    private static final int NUM_COL_SIEGE = 6;


    private SQLiteDatabase bdd;

    private MediaSQLite mediaSQLite;

    public MediaBDD(Context context){
        //On crée la BDD et sa table
        mediaSQLite = new MediaSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = mediaSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertMedia(Media media){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_LOGIN, media.getLogin());
        values.put(COL_PASSWORD, media.getPassword());
        values.put(COL_NAME, media.getName());
        values.put(COL_EMAIL, media.getEmail());
        values.put(COL_TYPE, media.getType());
        values.put(COL_SIEGE, media.getSiege());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_MEDIA, null, values);
    }

    public int updateMedia(int id, Media media){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_LOGIN, media.getLogin());
        values.put(COL_PASSWORD, media.getPassword());
        values.put(COL_NAME, media.getName());
        values.put(COL_EMAIL, media.getEmail());
        values.put(COL_TYPE, media.getType());
        values.put(COL_SIEGE, media.getSiege());
        return bdd.update(TABLE_MEDIA, values, COL_ID + " = " +id, null);
    }

    public int removMediaWithID(int id){
        //Suppression d'une institution de la BDD grâce à l'ID
        return bdd.delete(TABLE_MEDIA, COL_ID + " = " +id, null);
    }

    public Media getMediaWithLoginAndPassword(String login, String password){
        //Récupère dans un Cursor les valeurs correspondant à une institution contenue dans la BDD (ici on sélectionne l'institution grâce à son login et son password)
        Cursor c = bdd.query(TABLE_MEDIA, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD, COL_NAME, COL_EMAIL, COL_TYPE, COL_SIEGE}, COL_LOGIN + " LIKE \"" + login +"\""+ " AND " + COL_PASSWORD +" LIKE \"" + password +"\"" , null, null, null, null);
        return cursorToMedia(c);
    }

    public Media getMediaWithName(String nom){
        //Récupère dans un Cursor les valeurs correspondant à une institution contenue dans la BDD (ici on sélectionne l'institution grâce à son nom)
        Cursor c = bdd.query(TABLE_MEDIA, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD, COL_NAME, COL_EMAIL, COL_TYPE, COL_SIEGE}, COL_NAME + " LIKE \"" + nom +"\"", null, null, null, null);
        return cursorToMedia(c);
    }

    public Media getMediaWithEmail(String email){
        //Récupère dans un Cursor les valeurs correspondant à une institution contenue dans la BDD (ici on sélectionne l'institution grâce à son email)
        Cursor c = bdd.query(TABLE_MEDIA, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD, COL_NAME, COL_EMAIL, COL_TYPE, COL_SIEGE}, COL_EMAIL + " LIKE \"" + email +"\"", null, null, null, null);
        return cursorToMedia(c);
    }

    public Media getMediaWithType(String type){
        //Récupère dans un Cursor les valeurs correspondant à une institution contenue dans la BDD (ici on sélectionne l'institution grâce à son type)
        Cursor c = bdd.query(TABLE_MEDIA, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD, COL_NAME, COL_EMAIL, COL_TYPE, COL_SIEGE}, COL_TYPE + " LIKE \"" + type +"\"", null, null, null, null);
        return cursorToMedia(c);
    }

    public Media getMediaWithSiege(String siege){
        //Récupère dans un Cursor les valeurs correspondant à une institution contenue dans la BDD (ici on sélectionne l'institution grâce à son siege)
        Cursor c = bdd.query(TABLE_MEDIA, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD, COL_NAME, COL_EMAIL, COL_TYPE, COL_SIEGE}, COL_SIEGE + " LIKE \"" + siege +"\"", null, null, null, null);
        return cursorToMedia(c);
    }
    public Media getMediaWithLogin(String login){
        //Récupère dans un Cursor les valeurs correspondant à une institution contenue dans la BDD (ici on sélectionne le livre grâce à son titre)
        Cursor c = bdd.query(TABLE_MEDIA, new String[] {COL_ID, COL_LOGIN, COL_PASSWORD, COL_NAME, COL_EMAIL, COL_TYPE, COL_SIEGE}, COL_LOGIN + " LIKE \"" + login +"\"", null, null, null, null);
        return cursorToMedia(c);
    }
    //Cette méthode permet de convertir un cursor en une media
    private Media cursorToMedia(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un livre
        Media media = new Media();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        media.setId(c.getInt(NUM_COL_ID));
        media.setLogin(c.getString(NUM_COL_LOGIN));
        media.setPassword(c.getString(NUM_COL_PASSWORD));
        media.setName(c.getString(NUM_COL_NAME));
        media.setEmail(c.getString(NUM_COL_EMAIL));
        media.setType(c.getString(NUM_COL_TYPE));
        media.setSiege(c.getString(NUM_COL_SIEGE));
        //On ferme le cursor
        c.close();

        //On retourne l'institution
        return media;
    }
}
