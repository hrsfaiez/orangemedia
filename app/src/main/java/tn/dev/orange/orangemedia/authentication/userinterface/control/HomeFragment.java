package tn.dev.orange.orangemedia.authentication.userinterface.control;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachingListener;

public class HomeFragment extends Fragment {

    private IOnIntroHomeFragmentInteractionListener mListener;

    private Button loginButton;
    private Button registerButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_fragment_home, container, false);
        loginButton = (Button) view.findViewById(R.id.intro_home_login_button);
        registerButton = (Button) view.findViewById(R.id.intro_home_register_button);
        attachListenersToViews();
        return view;
    }

    /**
     * Attaches action listeners to views
     */
    private void attachListenersToViews() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onLoginPressed();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onRegisterPressed();
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachingListener) activity).getFragmentMediator();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentAttachingListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Creates an intro fragment instance
     *
     * @return an intro fragment instance
     */
    public static HomeFragment createInstance() {
        return new HomeFragment();
    }

    /***
     * This interface must be implemented by a mediator
     * of the activity that contains this fragment to
     * allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in it.
     */
    public interface IOnIntroHomeFragmentInteractionListener {
        /**
         * Callback to the event triggered when the user wants to log in
         */
        public void onLoginPressed();
        /**
         * Callback to the event triggered when the user wants to register
         */
        public void onRegisterPressed();
    }
}
