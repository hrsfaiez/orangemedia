package tn.dev.orange.orangemedia.media.userinterface.viewholder;


import tn.dev.orange.orangemedia.media.userinterface.control.IMediaViewHolderFactory;

public class ViewHolderFactory implements IMediaViewHolderFactory {
    @Override
    public MediaItemViewHolder createMediaItemViewHolder() {
        MediaItemViewHolder mediaItemViewHolder = new MediaItemViewHolder();
        return mediaItemViewHolder;
    }
}
