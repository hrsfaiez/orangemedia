package tn.dev.orange.orangemedia.user.userinterface.viewholder;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.IUpdateUserModelController;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.user.userinterface.viewdatamodel.UserProfileViewModel;

/**
 * This class holds the view elements of the form
 * that allows the user to modify his data model.
 * @<p>
 *      This is a helper class of the profile edit
 *      controller and should not be used outside
 *      that context.
 * </p>
 */
public class UserProfileEditViewHolder
        implements IUserViewHolder<User, IUpdateUserModelController> {
    /**
     * View elements of the form.
     */
    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private EditText emailEditText;
    private EditText addressEditText;
    private Button submitButton;

    /**
     * Initiates the view elements using a given
     * container view.
     * @param view the view of the form container layout.
     */
    @Override
    public void updateView (View view) {
        firstNameEditText
                = (EditText) view.findViewById(R.id.editProfile_firstName_editText);
        lastNameEditText
                = (EditText) view.findViewById(R.id.editProfile_lastName_editText);
        usernameEditText
                = (EditText) view.findViewById(R.id.editProfile_username_editText);
        passwordEditText
                = (EditText) view.findViewById(R.id.editProfile_password_editText);
        emailEditText
                = (EditText) view.findViewById(R.id.editProfile_email_editText);
        addressEditText
                = (EditText) view.findViewById(R.id.editProfile_address_editText);
        submitButton
                = (Button) view.findViewById(R.id.editProfile_submit_button);
    }

    /**
     * Changes the view elements values to the
     * given user model values.
     * @param user the user model to be used
     *             to fill the form.
     */
    @Override
    public void setUpModel(User user) {
        firstNameEditText.setText(user.getFistName());
        lastNameEditText.setText(user.getLastName());
        usernameEditText.setText(user.getUsername());
        //viewHolder.passwordEditText.setText("");
        emailEditText.setText(user.getEmail());
        addressEditText.setText(user.getAddress());
    }

    /**
     * Attaches an event listener to the form submit event.
     * @<p>
     *      Creates a user view model and passes it to the event listener.
     * </p>
     * @param updateUserModelController the listener of the form submit event.
     * @see UserProfileViewModel
     */
    @Override
    public void attachController(final IUpdateUserModelController updateUserModelController) {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstName = firstNameEditText.getText().toString();
                String lastName = lastNameEditText.getText().toString();
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                String email = emailEditText.getText().toString();
                String address = addressEditText.getText().toString();

                UserProfileViewModel userProfileViewModel
                        = UserProfileViewModel.newInstance(firstName, lastName, username
                        , password, email, address);
                userProfileViewModel.clean();

                updateUserModelController.updateUserModel(userProfileViewModel);
            }
        });
    }
}
