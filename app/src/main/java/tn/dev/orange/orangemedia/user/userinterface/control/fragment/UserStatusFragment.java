package tn.dev.orange.orangemedia.user.userinterface.control.fragment;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.user.domain.service.IUserService;
import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolderFactory;
import tn.dev.orange.orangemedia.user.domain.service.UserService;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.user.userinterface.viewholder.ViewHolderFactory;

//TODO:deal with that garbage
public class UserStatusFragment extends Fragment {

    //TODO:inject this
    public final IUserService modelProvider = UserService.createUserModelProvider();
    //TODO:inject this
    public final IUserViewHolderFactory viewHolderFactory = new ViewHolderFactory();

    private IUserViewHolder viewHolder;
    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        viewHolder = viewHolderFactory.createUserStatusViewHolder();
        user = modelProvider.getCurrentUser();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment_userstatus, container, false);
        viewHolder.updateView(view);
        viewHolder.setUpModel(user);
        viewHolder.attachController(this);
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        viewHolder = null;
        user = null;
    }
    /**
     * Creates a user status widget fragment
     *
     * @return a user status fragment instance
     */
    public static UserStatusFragment newInstance() {
        UserStatusFragment fragment = new UserStatusFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

}
