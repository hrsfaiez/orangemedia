package tn.dev.orange.orangemedia.authentication.userinterface.control;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel.LoginViewModel;
import tn.dev.orange.orangemedia.authentication.services.LoginService;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachingListener;
import tn.dev.orange.orangemedia.authentication.services.receivers.LoginResultReceiver;

//TODO:save fields values
//TODO:restore fields values
public class LoginFragment extends Fragment
    implements LoginResultReceiver.IOnLoginActionComplete {

    private IFragmentAttachingListener mListener;

    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button submitButton;
    private TextView recoverPasswordTextView;

    public LoginFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intro_fragment_login, container, false);
        usernameEditText = (EditText) view.findViewById(R.id.intro_login_username_editText);
        passwordEditText = (EditText) view.findViewById(R.id.intro_login_password_editText);
        submitButton = (Button) view.findViewById(R.id.intro_login_submit_button);
        recoverPasswordTextView = (TextView) view.findViewById
                (R.id.intro_login_forgetPassword_textView);
        attachListenersToViews();
        return view;
    }

    /**
     * Attaches action listeners to views
     */
    private void attachListenersToViews() {
        recoverPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.getFragmentMediator().onRecoverPasswordPressed();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                Parcelable loginParcelable = createLoginModel(username, password);
                login(loginParcelable);
            }
        });
    }

    /**
     * Launches a service to login the user
     * <p>
     * the result of the login are not handled here
     * </p>
     *
     * @param loginParcelable a login model
     * @see LoginResultReceiver.IOnLoginActionComplete
     */
    public void login (Parcelable loginParcelable) {
        Parcelable resultReceiver = createIntroLoginResultReceiver();

        Context context = getActivity();
        Intent intent = new Intent(context, LoginService.class);
        intent.setAction(LoginService.ACTION_LOGIN);
        intent.putExtra(LoginService.EXTRA_LOGIN_MODEL, loginParcelable);
        intent.putExtra(LoginService.EXTRA_LOGIN_RESULTRECEIVER, resultReceiver);
        context.startService(intent);

        //TODO:disabling the login button
    }

    /**
     * Creates the result receiver to handle login service result
     *
     * @return a login result receiver
     */
    private LoginResultReceiver createIntroLoginResultReceiver() {
        Handler handler = new Handler();
        return new LoginResultReceiver(handler, this);
    }

    /**
     * Creates a parcelable login model
     *
     * @param username the username of the user
     * @param password the password of the user
     * @return a login model
     */
    private LoginViewModel createLoginModel(String username, String password) {
        return new LoginViewModel(username, password);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachingListener) activity);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentAttachingListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        //TODO:act politely with the service
    }

    @Override
    public void onLoginSuccess() {
        mListener.getFragmentMediator().onLoginSuccess();
    }

    @Override
    public void onLoginFailure() {
        //TODO:make the inputs red
    }

    /**
     * Creates a login fragment instance
     *
     * @return a login fragment instance
     */
    public static LoginFragment createInstance () {
        return new LoginFragment();
    }

    /***
     * This interface must be implemented by the mediator
     * of the activity that contain this fragment to
     * allow in interaction with this fragment to be communicated
     * to the activity and potentially other fragments contained in it.
     */
    public interface ILoginFragmentInteractionListener {
        /**
         * Callback to the event triggered when the user logs in successfully
         */
        public void onLoginSuccess();
        /**
         * Callback to the event triggered when the user wants to recover this password
         */
        public void onRecoverPasswordPressed();
    }
}