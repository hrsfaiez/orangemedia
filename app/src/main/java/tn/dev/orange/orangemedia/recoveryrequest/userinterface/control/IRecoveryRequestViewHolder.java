package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control;

import android.view.View;

public interface IRecoveryRequestViewHolder<TModel, TController> {
    void updateView (View view);
    void setUpModel(TModel model);
    void attachController(final TController controller);
}
