package tn.dev.orange.orangemedia.media.domain.datamodel;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * a media model
 */
public class Media implements Parcelable {
    private String name;
    private Date accreditationDate;

    public Media(String name, Date accreditationDate) {
        this.name = name;
        this.accreditationDate = accreditationDate;
    }

    public Media(Parcel in) {
        setName(in.readString());
        Date accreditionDate = new Date(in.readLong());
        setAccreditDate(accreditionDate);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getAccreditDate() {
        return accreditationDate;
    }

    public void setAccreditDate(Date accreditationDate) {
        this.accreditationDate = accreditationDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getName());
        destination.writeLong(getAccreditDate().getTime());
    }

    public static final Parcelable.Creator<Media> CREATOR
            = new Parcelable.Creator<Media>() {
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }


        public Media[] newArray(int size) {
            return new Media[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Media)) {
            return false;
        }
        if (getName().equals(((Media) o).getName())
                && getAccreditDate().equals(((Media) o).getAccreditDate())) {
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        String mediaString = getName();
        return mediaString;
    }
}
