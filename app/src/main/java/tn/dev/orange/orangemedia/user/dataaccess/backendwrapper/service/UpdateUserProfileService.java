package tn.dev.orange.orangemedia.user.dataaccess.backendwrapper.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.contribution.dataaccess.service.AddContributionService;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

public class UpdateUserProfileService extends IntentService {
    /*
     * Update user profile action name
     */
    public static final String ACTION_UPDATEPROFILE = "ACTION_UPDATEPROFILE";

    /*
     * Update user profile action parameters
     */
    public static final String EXTRA_UPDATEPROFILE_MODEL = "EXTRA_UPDATEPROFILE_MODEL";
    public static final String EXTRA_UPDATEPROFILE_RESULTRECEIVER
            = "EXTRA_UPDATEPROFILE_RESULTRECEIVER";

    /*
     * Update user informations result codes
     */
    public static final int UPDATEINFORMATIONS_SUCCESS = 1;
    public static final int UPDATEINFORMATIONS_FAILURE = 0;
    //TODO:checking for connection out failures ?

    public UpdateUserProfileService() {
        super("UpdateUserProfileService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATEPROFILE.equals(action)) {
                // handles the user update

                // retrieves the user view model
                final User user
                        = intent.getParcelableExtra(EXTRA_UPDATEPROFILE_MODEL);
                // retrieves the service result receiver
                final ResultReceiver resultReceiver = intent.
                        getParcelableExtra(EXTRA_UPDATEPROFILE_RESULTRECEIVER);
                // handles the profile update
                handleUpdateProfile(user, resultReceiver);
            }
        }
    }

    /**
     * Sends the user new profile to the back-end
     *
     * @param user the new user model
     * @param resultReceiver       the service result receiver
     */
    private void handleUpdateProfile(User user,
                                     ResultReceiver resultReceiver) {
        // TODO: request_d a web service to update user informations, then call the result receiver callback
        /*
         * just some glue code
         */
        resultReceiver.send(UPDATEINFORMATIONS_SUCCESS, new Bundle());
    }

    /**
     * Starts a service action for a user profile update
     *
     * @param context               the context of the action source
     * @param contributionViewModel the user view model
     * @param resultReceiver        the service result receiver
     */
    public static void startActionUpdateUserInformations(Context context,
                                                         Parcelable contributionViewModel,
                                                         ResultReceiver resultReceiver) {
        Intent intent = new Intent(context, AddContributionService.class);
        intent.setAction(ACTION_UPDATEPROFILE);
        intent.putExtra(EXTRA_UPDATEPROFILE_MODEL, contributionViewModel);
        intent.putExtra(EXTRA_UPDATEPROFILE_RESULTRECEIVER, resultReceiver);
        context.startService(intent);
    }

}
