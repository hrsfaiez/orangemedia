package tn.dev.orange.orangemedia.user.domain.service;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import tn.dev.orange.orangemedia.user.userinterface.control.fragment.IOnUserModelUpdateComplete;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.user.dataaccess.backendwrapper.resultreciever.UserProfileEditResultReceiver;
import tn.dev.orange.orangemedia.user.dataaccess.backendwrapper.service.UpdateUserProfileService;

public class UserService implements IUserService {

    /**
     * Creates a user model provider
     *
     * @return a user data model provider instance
     */
    public static UserService createUserModelProvider() {
        return new UserService();
    }

    @Override
    public User getCurrentUser() {
        User user = new User("firstname", "lastname", "username", "passwordmd5", "email", "address");
        return user;
    }

    @Override
    public void updateCurrentUserModel(User newUser, Context context,
                                       IOnUserModelUpdateComplete userModelUpdateCompleteListener) {
        Handler handler = new Handler();
        UserProfileEditResultReceiver resultReceiver =
                new UserProfileEditResultReceiver(handler, userModelUpdateCompleteListener);

        //TODO: try to omit the next line and use a couchedb sync
        UpdateUserProfileService.startActionUpdateUserInformations(context,
                newUser, resultReceiver);
        //TODO:check if the pasword is null
        Log.d("NEWUSER", newUser.toString());
    }
}
