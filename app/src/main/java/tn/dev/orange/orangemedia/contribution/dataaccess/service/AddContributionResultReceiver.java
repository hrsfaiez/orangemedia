package tn.dev.orange.orangemedia.contribution.dataaccess.service;


import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AddContributionResultReceiver extends ResultReceiver {

    /**
     * Contribution service listener
     */
    private IOnContributeActionComplete onContributeActionCompleteListener;

    public AddContributionResultReceiver(Handler handler,
                             IOnContributeActionComplete onContributeActionCompleteListener) {
        super(handler);
        this.onContributeActionCompleteListener = onContributeActionCompleteListener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (null != getOnContributeActionCompleteListener()) {
            if (AddContributionService.CONTRIBUTE_SUCCESS == resultCode) {
                // the service contribution insert action succeed
                getOnContributeActionCompleteListener().onContributeServiceSuccess();
            } else if (AddContributionService.CONTRIBUTE_FAILURE == resultCode) {
                // the service contribution insert action failed
                getOnContributeActionCompleteListener().onContributeServiceFailure();
            }
        }
    }

    /**
     * Gets the listener of the contribution service
     *
     * @return the contribution service listener
     */
    public IOnContributeActionComplete getOnContributeActionCompleteListener() {
        return onContributeActionCompleteListener;
    }

    /**
     * Updates the listener of the contribution service
     *
     * @param onContributeActionCompleteListener the new listener of the contribution service
     */
    public void setOnContributeActionCompleteListener
            (IOnContributeActionComplete onContributeActionCompleteListener) {
        this.onContributeActionCompleteListener = onContributeActionCompleteListener;
    }

    /**
     * Interface must be implemented by the class
     * that handles the result of the service responsible for
     * sending the user contributions.
     */
    public interface IOnContributeActionComplete {
        /**
         * Callback to event triggered when the contribution insertion succeeds
         */
        public void onContributeServiceSuccess();
        /**
         * Callback to event triggered when the contribution insertion fails
         */
        public void onContributeServiceFailure();
    }

    public static AddContributionResultReceiver createAddContributionResultReceiver (Handler handler,
                                     IOnContributeActionComplete onContributeActionCompleteListener) {
        return new AddContributionResultReceiver(handler, onContributeActionCompleteListener);
    }
}
