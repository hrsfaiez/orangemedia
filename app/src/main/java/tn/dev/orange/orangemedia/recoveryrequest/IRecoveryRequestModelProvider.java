package tn.dev.orange.orangemedia.recoveryrequest;


import java.util.List;

import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

public interface IRecoveryRequestModelProvider {
    List<RecoveryRequest> getActiveRecoveryRequests (User user);
    void deleteRequest(RecoveryRequest recoveryRequest);
}
