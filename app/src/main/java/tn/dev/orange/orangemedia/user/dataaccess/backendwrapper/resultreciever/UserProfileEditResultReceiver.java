package tn.dev.orange.orangemedia.user.dataaccess.backendwrapper.resultreciever;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.user.userinterface.control.fragment.IOnUserModelUpdateComplete;
import tn.dev.orange.orangemedia.user.dataaccess.backendwrapper.service.UpdateUserProfileService;

public class UserProfileEditResultReceiver extends ResultReceiver {

    /**
     * Profile update service listener
     */
    private IOnUserModelUpdateComplete userModelUpdateCompleteListener;

    public UserProfileEditResultReceiver(Handler handler,
                                         IOnUserModelUpdateComplete userModelUpdateCompleteListener) {
        super(handler);
        this.userModelUpdateCompleteListener = userModelUpdateCompleteListener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (null != getUserModelUpdateCompleteListener()) {
            if (UpdateUserProfileService.UPDATEINFORMATIONS_SUCCESS == resultCode) {
                // the service profile update action succeed
                getUserModelUpdateCompleteListener().onUserModelUpdateSuccess();
            } else if (UpdateUserProfileService.UPDATEINFORMATIONS_FAILURE == resultCode) {
                // the service profile update action failed
                getUserModelUpdateCompleteListener().onUserModelUpdateFailure();
            }
        }
    }

    /**
     * Gets the profile update service listener
     *
     * @return the profile update service listener
     */
    public IOnUserModelUpdateComplete getUserModelUpdateCompleteListener() {
        return userModelUpdateCompleteListener;
    }

}
