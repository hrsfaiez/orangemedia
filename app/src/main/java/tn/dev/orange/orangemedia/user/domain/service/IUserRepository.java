package tn.dev.orange.orangemedia.user.domain.service;


import tn.dev.orange.orangemedia.user.domain.datamodel.User;

public interface IUserRepository {
    void addUser (User user);
    void removeUser (User user);
}
