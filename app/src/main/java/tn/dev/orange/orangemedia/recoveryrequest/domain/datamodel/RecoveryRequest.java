package tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

import tn.dev.orange.orangemedia.media.domain.datamodel.Media;

/**
 * a recovery request_d model
 */
public final class RecoveryRequest implements Parcelable {
    private Media media;
    private String title;
    private String category;
    private Date date;
    private Date expirationDate;
    private String content;
    private RecoveryRequestState state;

    public RecoveryRequest(Media media, String title, String category, Date date,
                           Date expirationDate, String content, RecoveryRequestState state) {
        setMedia(media);
        setTitle(title);
        setCategory(category);
        setDate(date);
        setExpirationDate(expirationDate);
        setContent(content);
        setState(state);
    }

    public RecoveryRequest(Parcel in) {
        setMedia((Media) in.readSerializable());
        setTitle(in.readString());
        setCategory(in.readString());
        Date date = new Date(in.readLong());
        setDate(date);
        Date expirationDate = new Date(in.readLong());
        setExpirationDate(expirationDate);
        setContent(in.readString());
        RecoveryRequestState recoveryRequestState = extractRecoveryState(in.readInt());
        setState(recoveryRequestState);
    }

    public RecoveryRequestState extractRecoveryState(Integer recoveryRequestState) {
        if (RecoveryRequestState.seen_responded.getValue().equals(recoveryRequestState)) {
            return RecoveryRequestState.seen_responded;
        } else if (RecoveryRequestState.seen_notResponded.getValue().equals(recoveryRequestState)){
            return RecoveryRequestState.seen_notResponded;
        } else {
            return RecoveryRequestState.notSeen;
        }
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public RecoveryRequestState getState() {
        return state;
    }

    public void setState(RecoveryRequestState state) {
        this.state = state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeSerializable((Serializable) media);
        destination.writeString(getTitle());
        destination.writeString(getCategory());
        destination.writeLong(getDate().getTime());
        destination.writeLong(getExpirationDate().getTime());
        destination.writeString(getContent());
        destination.writeInt(getState().getValue());
    }

    public static final Parcelable.Creator<RecoveryRequest> CREATOR
            = new Parcelable.Creator<RecoveryRequest>() {
        public RecoveryRequest createFromParcel(Parcel in) {
            return new RecoveryRequest(in);
        }


        public RecoveryRequest[] newArray(int size) {
            return new RecoveryRequest[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RecoveryRequest)) {
            return false;
        }
        if (getMedia().equals(((RecoveryRequest) o).getMedia())
                && getTitle().equals(((RecoveryRequest) o).getTitle())
                && getCategory().equals(((RecoveryRequest) o).getCategory())
                && getDate().equals(((RecoveryRequest) o).getDate())
                && getExpirationDate().equals(((RecoveryRequest) o).getExpirationDate())
                && getContent().equals(((RecoveryRequest) o).getContent())) {
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        return getTitle();
    }
}
