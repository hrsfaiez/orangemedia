package tn.dev.orange.orangemedia.contribution.userinterface.view;


import tn.dev.orange.orangemedia.contribution.userinterface.view.viewmodel.ContributionViewModel;

public interface ISendContributionController {
    public void contribute (ContributionViewModel contributionViewModel);
}
