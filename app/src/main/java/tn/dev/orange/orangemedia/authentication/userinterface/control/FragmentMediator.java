package tn.dev.orange.orangemedia.authentication.userinterface.control;


import tn.dev.orange.orangemedia.appmodel.HomeActivity;

/***
 * Mediator between fragments and intro activity
 * this class is a listeners to all translation
 * actions of fragments
 */
public class FragmentMediator
        implements HomeFragment.IOnIntroHomeFragmentInteractionListener,
        LoginFragment.ILoginFragmentInteractionListener,
        RegistrationFragment.IInscriptionFragmentInteractionListener,
        RecoveringPasswordFragment.IRecoveringPasswordFragmentInteractionListener {

    private IFragmentTransactor introFragmentTransactor;
    private IActivityMainSwitchListener activityMainSwitchListener;


    public FragmentMediator(IFragmentTransactor introFragmentTransactor,
                            IActivityMainSwitchListener activityMainSwitchListener) {
        this.introFragmentTransactor = introFragmentTransactor;
        this.activityMainSwitchListener = activityMainSwitchListener;
    }

    @Override
    public void onLoginPressed() {
        introFragmentTransactor.switchToLogin();
    }

    @Override
    public void onRegisterPressed() {
        introFragmentTransactor.switchToRegister();
    }

    @Override
    public void onLoginSuccess() {
        //TODO: implementing login session logic
        Class<HomeActivity> mainActivity = HomeActivity.class;
        activityMainSwitchListener.switchToHomeActivity(mainActivity);
    }

    @Override
    public void onRecoverPasswordPressed() {
        introFragmentTransactor.switchToRecoverPassword();
    }

    @Override
    public void onRegisterSuccess() {
        //TODO: implementing registration success logic
    }

    @Override
    public void onRecoverPasswordSuccess() {
        //TODO: implementing onRecoverPasswordSuccess logic
    }

    /**
     * This interface must be implemented by the class
     * responsible for switching between the current activity
     * and the application main activity.
     */
    public interface IActivityMainSwitchListener {
        /**
         * Callback to the event triggered when a switch to the main activity is needed
         */
        void switchToHomeActivity(Class<HomeActivity> mainActivity);
    }
}
