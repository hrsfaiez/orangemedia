package tn.dev.orange.orangemedia.media.dataaccess.dataprovider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import tn.dev.orange.orangemedia.media.domain.datamodel.Media;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.media.IMediaModelProvider;

public class MediaModelProvider implements IMediaModelProvider {

    /**
     * Creates a media model provider
     *
     * @return a media model provider instance
     */
    public static MediaModelProvider newInstance() {
        return new MediaModelProvider();
    }

    @Override
    public List<Media> getAccreditedMedias(User user) {
        List<Media> mediaList = new ArrayList<Media>();
        mediaList.add(new Media("media_1name", new Date()));
        mediaList.add(new Media("media_21name", new Date()));
        return mediaList;
    }
}
