package tn.dev.orange.orangemedia.appmodel.navigationmodel;

import android.view.Menu;
import android.view.MenuItem;

import tn.dev.orange.orangemedia.appmodel.IOnActionBarInteraction;

public interface IActionBarActionStateHolder {
    void switchToNewState(MenuItem item);
    void updateMenu (Menu menu);
    void updateListener (IOnActionBarInteraction actionBarInteractionListener);
}
