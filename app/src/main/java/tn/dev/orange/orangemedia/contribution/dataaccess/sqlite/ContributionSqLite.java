package tn.dev.orange.orangemedia.contribution.dataaccess.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BSI on 10/06/2015.
 */
public class ContributionSqLite extends SQLiteOpenHelper {
    private static final String TABLE_CONTRIBUTION = "table_contributions";
    private static final String COL_ID = "ID";
    private static final String COL_OBJET ="OBJET";
    private static final String COL_TEXT = "TEXT";
    private static final String COL_IMAGE = "IMAGE";
    private static final String COL_DATE = "DATE";
    private static final String COL_LOCALISATION = "LOCALISATION";
    private static final String COL_ID_REPORTEU = "ID_REPORTEUR";
    private static final String COL_ID_DEMANDE = "ID_DEMANDE";
    private static final String CREATE_BDD = "CREATE TABLE "+ TABLE_CONTRIBUTION + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_OBJET + " TEXT NOT NULL, "
            + COL_TEXT + " TEXT NOT NULL, "+COL_IMAGE + " TEXT NOT NULL, "+ COL_DATE+ " TEXT NOT NULL, "
            + COL_LOCALISATION+  " TEXT NOT NULL, "+ COL_ID_REPORTEU+ " TEXT NOT NULL, "+
            COL_ID_DEMANDE+ " TEXT NOT NULL);";
    public ContributionSqLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {

        super(context,name,factory,version);
    }






    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
        sqLiteDatabase.execSQL(CREATE_BDD);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        //on supprime la table et on la recrée
        //comme ça lorsque je change la version les id repartent de 0
        sqLiteDatabase.execSQL("DROP TABLE "+ TABLE_CONTRIBUTION +" ;");
        onCreate(sqLiteDatabase);

    }
}
