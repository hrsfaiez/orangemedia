package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.recoveryrequest.IOnRecoveryRequestInteractionListener;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolder;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.IRecoveryRequestViewHolderFactory;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachedListener;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.viewholder.ViewHolderFactory;

public class RecoveryRequestFragment extends Fragment
        implements IAcceptRecoveryRequestController {

    //TODO:inject this
    public final IRecoveryRequestViewHolderFactory viewHolderFactory = new ViewHolderFactory();

    private IRecoveryRequestViewHolder viewHolder;
    private RecoveryRequest recoveryRequest;
    private IOnRecoveryRequestInteractionListener mListener;

    private MainActivityViewState requiredViewState;

    public final MainActivityViewState getRequiredViewStyle() {
        return requiredViewState;
    }

    @Override
    public void acceptRecoveryRequest(RecoveryRequest recoveryRequest) {
        mListener.respondToRecoveryRequest(recoveryRequest);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        viewHolder = viewHolderFactory.createRecoveryRequestViewHolder();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment_recoveryrequest, container, false);

        recoveryRequest = (RecoveryRequest) getArguments().get(RECOVERY_REQUEST_ARG_NAME);
        viewHolder.updateView(view);
        viewHolder.setUpModel(recoveryRequest);
        viewHolder.attachController(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachedListener) activity).getListener();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement IFragmentAttachedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        viewHolder = null;
        recoveryRequest = null;
    }

    /**
     * The recovery request_d model bundle key
     */
    public static final String RECOVERY_REQUEST_ARG_NAME = "RECOVERY_REQUEST_ARG_NAME";

    /**
     * Creates a recovery request_d fragment
     *
     * @return a recovery request_d fragment instance
     */
    public static RecoveryRequestFragment newInstance() {
        RecoveryRequestFragment fragment = new RecoveryRequestFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        fragment.requiredViewState = MainActivityViewState.USER_STATUS_VIEW_STYLE;
        return fragment;
    }
}
