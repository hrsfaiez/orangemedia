package tn.dev.orange.orangemedia.authentication.services.receivers;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.authentication.services.RecoveringPasswordService;

public class RecoveringPasswordResultReceiver extends ResultReceiver {
    private IOnRecoverPasswordActionComplete onRecoverPasswordCheckCompleteListener;

    public RecoveringPasswordResultReceiver(Handler handler,
                        IOnRecoverPasswordActionComplete onRecoverPasswordCheckCompleteListener) {
        super(handler);
        this.onRecoverPasswordCheckCompleteListener = onRecoverPasswordCheckCompleteListener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (null != onRecoverPasswordCheckCompleteListener) {
            if (RecoveringPasswordService.RECOVER_PASSWORD_SUCCESS == resultCode) {
                onRecoverPasswordCheckCompleteListener.onRecoverPasswordSuccess();
            }
        }
    }

    /***
     * Interface must be implemented by a view
     * that handles the result of the service responsible for
     * checking the validity of the login.
     */
    public interface IOnRecoverPasswordActionComplete {
        /**
         * Callback to the event triggered when the sent of the recovering request_d succeed
         */
        public void onRecoverPasswordSuccess ();
    }

}
