package tn.dev.orange.orangemedia.authentication.services.receivers;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.authentication.services.LoginService;


public class RegistrationResultReceiver extends ResultReceiver {

    private IOnRegistrationActionComplete onLRegistrationCheckCompleteListener;

    public RegistrationResultReceiver(Handler handler,
                              IOnRegistrationActionComplete onLRegistrationCheckCompleteListener) {
        super(handler);
        this.onLRegistrationCheckCompleteListener = onLRegistrationCheckCompleteListener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (null != onLRegistrationCheckCompleteListener) {
            if (LoginService.LOGIN_SUCCESS == resultCode) {
                onLRegistrationCheckCompleteListener.onRegistrationSuccess();
            } else if (LoginService.LOGIN_FAILURE == resultCode) {
                onLRegistrationCheckCompleteListener.onRegistrationFailure();
            }
        }
    }

    /***
     * Interface must be implemented by a view
     * that handles the result of the service responsible for
     * inserting the user.
     */
    public interface IOnRegistrationActionComplete {
        /**
         * Callback to the event triggered when the registration succeed
         */
        public void onRegistrationSuccess();

        /**
         * Callback to the event triggered when the registration fails
         */
        public void onRegistrationFailure();
    }
}
