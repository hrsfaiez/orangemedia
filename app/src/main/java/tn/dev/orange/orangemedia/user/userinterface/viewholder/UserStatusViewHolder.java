package tn.dev.orange.orangemedia.user.userinterface.viewholder;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.UserStatusFragment;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

public class UserStatusViewHolder
        implements IUserViewHolder<User, UserStatusFragment> {

    private TextView nameTextView;
    private TextView locationTextView;
    private ImageView userPictureImageView;

    @Override
    public void updateView(View view) {
        nameTextView = (TextView) view.findViewById(R.id.userStatus_name_textview);
        locationTextView = (TextView) view.findViewById(R.id.userStatus_location_textView);
        userPictureImageView = (ImageView) view.findViewById(R.id.userStatus_picture_imageView);

    }

    @Override
    public void setUpModel(User user) {
        nameTextView.setText(user.getName());
        locationTextView.setText(user.getLocation().getName());
    }

    @Override
    public void attachController(UserStatusFragment userStatusFragment) {

    }
}
