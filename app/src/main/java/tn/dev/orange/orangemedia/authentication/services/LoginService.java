package tn.dev.orange.orangemedia.authentication.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import tn.dev.orange.orangemedia.authentication.userinterface.veiwmodel.LoginViewModel;


public class LoginService extends IntentService {
    /*
     * Login action name
     */
    public static final String ACTION_LOGIN = "ACTION_LOGIN";

    /*
     * Login action parameters
     */
    public static final String EXTRA_LOGIN_MODEL = "EXTRA_LOGIN_MODEL";
    public static final String EXTRA_LOGIN_RESULTRECEIVER = "EXTRA_LOGIN_RESULT_RECEIVER";

    /*
     * Login result codes
     */
    public static final int LOGIN_SUCCESS = 1;
    public static final int LOGIN_FAILURE = 0;
    //TODO:checking for connection out failures ?

    public LoginService() {
        super("LoginCheckerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (null != intent) {
            final String action = intent.getAction();
            if (ACTION_LOGIN.equals(action)) {
                final LoginViewModel loginModel = intent.getParcelableExtra(EXTRA_LOGIN_MODEL);
                final ResultReceiver resultReceiver = intent.
                        getParcelableExtra(EXTRA_LOGIN_RESULTRECEIVER);
                handleActionLogin(loginModel, resultReceiver);
            }
        }
    }

    /**
     * Checks the validity of the login and passes the result back to the listener
     *
     * @param loginModel a login model
     * @param resultReceiver a listener to the result of the login
     * @see android.os.ResultReceiver
     */
    private void handleActionLogin(LoginViewModel loginModel, ResultReceiver resultReceiver) {
        // TODO: request_d a web service to check the login, then call the result receiver callback
        /*
         * just some glue code
         */
        resultReceiver.send(LOGIN_SUCCESS, new Bundle());
    }
}
