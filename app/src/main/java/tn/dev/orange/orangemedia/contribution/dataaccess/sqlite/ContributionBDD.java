package tn.dev.orange.orangemedia.contribution.dataaccess.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by BSI on 11/06/2015.
 */
public class ContributionBDD  {
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "orangeMedia.db";
    private static final String TABLE_CONTRIBUTION = "table_contributions";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_OBJET ="OBJET";
    private static final int NUM_COL_OBJET  = 1;
    private static final String COL_TEXT = "TEXT";
    private static final int NUM_COL_TEXT  = 2;
    private static final String  COL_IMAGE = "IMAGE";
    private static final int NUM_COL_IMAGE  = 3;
    private static final String COL_DATE = "DATE";
    private static final int NUM_COL_DATE  = 4;
    private static final String COL_LOCALISATION = "LOCALISATION";
    private static final int NUM_COL_LOCALISATION = 5;
    private static final String  COL_ID_REPORTEU = "ID_REPORTEUR";
    private static final int NUM_COL_ID_REPORTEU = 6;
    private static final String  COL_ID_DEMANDE = "ID_DEMANDE";
    private static final int NUM_COL_ID_DEMANDE = 7;

    private SQLiteDatabase bdd;

    private ContributionSqLite contributionSqLite;


    public ContributionBDD(Context context){
        //On crée la BDD et sa table
        contributionSqLite = new ContributionSqLite(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = contributionSqLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertLivre(Contribution contribution){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
        values.put(COL_OBJET, contribution.getObjet());
        values.put(COL_TEXT, contribution.getText());
        values.put(COL_IMAGE, contribution.getImage());
        values.put(COL_DATE,contribution.getDate());
        values.put(COL_LOCALISATION, contribution.getLocalisation());
        values.put(COL_ID_REPORTEU, contribution.getId_Reporteur());
        values.put(COL_ID_DEMANDE, contribution.getId_Demande());

        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_CONTRIBUTION, null, values);
    }

    public int updateLivre(int id, Contribution contribution){
        //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
        ContentValues values = new ContentValues();
        values.put(COL_OBJET, contribution.getObjet());
        values.put(COL_TEXT, contribution.getText());
        values.put(COL_IMAGE, contribution.getImage());
        //values.put(COL_DATE,(String) contribution.getDate());
        values.put(COL_LOCALISATION, contribution.getLocalisation());
        values.put(COL_ID_REPORTEU, contribution.getId_Reporteur());
        values.put(COL_ID_DEMANDE, contribution.getId_Demande());

        return bdd.update(TABLE_CONTRIBUTION, values, COL_ID + " = " +id, null);
    }

    public int removeContributionWithID(int id){
        //Suppression d'une contribution de la BDD grâce à l'ID
        return bdd.delete(TABLE_CONTRIBUTION, COL_ID + " = " +id, null);
    }

    public Contribution getContributionWithObjet(String objet){
        //Récupère dans un Cursor les valeurs correspondant à une contribution contenue dans la BDD (ici on sélectionne la contribution grâce à son objet)
        Cursor c = bdd.query(TABLE_CONTRIBUTION, new String[] {COL_ID, COL_OBJET, COL_TEXT, COL_IMAGE , COL_DATE, COL_LOCALISATION, COL_ID_REPORTEU, COL_ID_DEMANDE}, COL_OBJET+ " LIKE \"" + objet +"\"", null, null, null, null);

        return cursorToContribution(c);
    }

    //Cette méthode permet de convertir un cursor en une contribution
    private Contribution cursorToContribution(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return null;

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé une contribution
        Contribution contribution = new Contribution();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        contribution.setId(c.getInt(NUM_COL_ID));
        contribution.setObjet(c.getString(NUM_COL_OBJET));
        contribution.setText(c.getString(NUM_COL_TEXT));
        contribution.setImage(c.getString(NUM_COL_IMAGE));
        contribution.setDate(c.getString(NUM_COL_DATE));
        contribution.setLocalisation(c.getString(NUM_COL_LOCALISATION));
        contribution.setId_Reporteur(c.getInt(NUM_COL_ID_REPORTEU));
        contribution.setId_Demande(c.getInt(NUM_COL_ID_DEMANDE));
        

        //On ferme le cursor
        c.close();

        //On retourne la contribution
        return contribution;
    }

    public Contribution getContributionWithLocalisation(String localisation){
        //Récupère dans un Cursor les valeurs correspondant à une contribution contenue dans la BDD (ici on sélectionne la contribution grâce à son objet)
        Cursor c = bdd.query(TABLE_CONTRIBUTION, new String[] {COL_ID, COL_OBJET, COL_TEXT, COL_IMAGE , COL_DATE, COL_LOCALISATION, COL_ID_REPORTEU, COL_ID_DEMANDE}, COL_LOCALISATION+ " LIKE \"" + localisation +"\"", null, null, null, null);

        return cursorToContribution(c);
    }


    public Contribution getContributionWithDate(String date){
        //Récupère dans un Cursor les valeurs correspondant à une contribution contenue dans la BDD (ici on sélectionne la contribution grâce à son objet)
        Cursor c = bdd.query(TABLE_CONTRIBUTION, new String[] {COL_ID, COL_OBJET, COL_TEXT, COL_IMAGE , COL_DATE, COL_LOCALISATION, COL_ID_REPORTEU, COL_ID_DEMANDE}, COL_DATE+ " LIKE \"" + date +"\"", null, null, null, null);

        return cursorToContribution(c);
    }



}
