package tn.dev.orange.orangemedia.user.userinterface.control;


import tn.dev.orange.orangemedia.user.userinterface.viewholder.UserProfileEditViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.viewholder.UserProfileViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.viewholder.UserStatusViewHolder;

public interface IUserViewHolderFactory {
    UserProfileEditViewHolder createUserProfileEditViewHolder();
    UserProfileViewHolder createUserProfileViewHolder();
    UserStatusViewHolder createUserStatusViewHolder();
}
