package tn.dev.orange.orangemedia.contribution.userinterface.view.viewmodel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ContributionViewModel implements Parcelable{
    private String title;
    private String content;
    private String category;
    private String target;
    private Date date;

    public ContributionViewModel(String title, String content, String category,
                                 String target, Date date) {
        this.title = title;
        this.content = content;
        this.category = category;
        this.target = target;
        this.date = date;
    }

    public ContributionViewModel(Parcel in) {
        setTitle(in.readString());
        setContent(in.readString());
        setCategory(in.readString());
        setTarget(in.readString());
        Date date1 = new Date(in.readLong());
        setDate(date1);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getTitle());
        destination.writeString(getContent());
        destination.writeString(getCategory());
        destination.writeString(getTarget());
        destination.writeLong(getDate().getTime());
    }

    public static final Parcelable.Creator<ContributionViewModel> CREATOR
            = new Parcelable.Creator<ContributionViewModel>() {
        public ContributionViewModel createFromParcel(Parcel in) {
            return new ContributionViewModel(in);
        }



        public ContributionViewModel[] newArray(int size) {
            return new ContributionViewModel[size];
        }
    };

    public static ContributionViewModel newInstance (String title, String content, String category,
        String target, Date date) {
        return new ContributionViewModel(title, content, category, target, date);
    }

}
