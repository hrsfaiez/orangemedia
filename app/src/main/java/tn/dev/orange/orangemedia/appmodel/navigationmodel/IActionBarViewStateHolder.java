package tn.dev.orange.orangemedia.appmodel.navigationmodel;

public interface IActionBarViewStateHolder {
    void switchToProfile();
    void switchToMedia();
    void switchToRecoveryRequest();
    void switchToContribution();
}
