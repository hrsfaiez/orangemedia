package tn.dev.orange.orangemedia.contribution.userinterface.control;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.faiezdev.helloandro.R;

import java.util.Date;

import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.contribution.dataaccess.service.AddContributionResultReceiver;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachedListener;
import tn.dev.orange.orangemedia.contribution.dataaccess.service.AddContributionService;
import tn.dev.orange.orangemedia.contribution.userinterface.view.ISendContributionController;
import tn.dev.orange.orangemedia.contribution.userinterface.view.viewmodel.ContributionViewModel;

public class ContributeFragment extends Fragment
        implements AddContributionResultReceiver.IOnContributeActionComplete,
                        ISendContributionController{

    private MainActivityViewState requiredViewState;

    public final MainActivityViewState getRequiredViewStyle() {
        return requiredViewState;
    }

    /**
     * The recovery request_d model
     */
    private RecoveryRequest recoveryRequest;

    public final static String REQUESTRECOVERY_ARG_NAME = "REQUESTRECOVERY_ARG_NAME";

    /**
     * The recovery request_d model bundle key
     */

    private ViewHolder viewHolder;

    private IOnContributionInteraction mListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment_addcontribution, container, false);

        if (null != getArguments()) {
            recoveryRequest = (RecoveryRequest) getArguments().get(REQUESTRECOVERY_ARG_NAME);
        }

        viewHolder = ViewHolder.createViewHolder(view);
        if (isResponse()) {
            viewHolder.setUpRecoveryRequestModel(recoveryRequest);
        } else {
            viewHolder.resetRecoveryRequestView();
        }

        viewHolder.attachActions(this);
        return view;
    }

    private boolean isResponse() {
        boolean hasRecoveryRequestModel = (null != recoveryRequest);
        return hasRecoveryRequestModel;
    }

    @Override
    public void contribute(ContributionViewModel contributionViewModel) {
        Parcelable contributionParcelable = (Parcelable) contributionViewModel;

        Handler handler = new Handler();
        ResultReceiver resultReceiver =
                AddContributionResultReceiver.createAddContributionResultReceiver(handler,this);
        Context context = getActivity();
        AddContributionService.startActionContribute(context,
                contributionParcelable, resultReceiver);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachedListener) activity).getListener();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onContributeServiceSuccess() {
        mListener.onContributeSuccess();
    }

    @Override
    public void onContributeServiceFailure() {
        //TODO:making fields red
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        viewHolder = null;
        recoveryRequest = null;
    }

    /**
     * Holder class of the fragment views
     */
    private static class ViewHolder {
        EditText titleEditText;
        EditText contentEditText;
        EditText categoryEditText;
        EditText targetEditText;
        //TODO:add pictures handling
        Button submitButton;

        private ViewHolder(View view) {
            titleEditText
                    = (EditText) view.findViewById(R.id.addContribution_title_textView);
            contentEditText
                    = (EditText) view.findViewById(R.id.addContribution_content_textView);
            categoryEditText
                    = (EditText) view.findViewById(R.id.addContribution_category_textView);
            targetEditText
                    = (EditText) view.findViewById(R.id.addContribution_target_textView);
            submitButton
                    = (Button) view.findViewById(R.id.addContribution_submit_button);
        }

        /**
         * Puts the recovery requests values inside the fragment views
         *
         * @param recoveryRequest the recovery request_d model
         */
        private void setUpRecoveryRequestModel(RecoveryRequest recoveryRequest) {
            titleEditText.setText(recoveryRequest.getTitle());
            categoryEditText.setText(recoveryRequest.getCategory());
            targetEditText.setText(recoveryRequest.getMedia().toString());
        }

        private void resetRecoveryRequestView () {
            //titleEditText.setText("");
            //categoryEditText.setText("");
            //targetEditText.setText("");
        }

        private void attachActions(final ISendContributionController sendContributionController) {
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String title = titleEditText.getText().toString();
                    String content = contentEditText.getText().toString();
                    String category = categoryEditText.getText().toString();
                    String target = targetEditText.getText().toString();
                    Date date = new Date();

                    ContributionViewModel contributionViewModel
                            = ContributionViewModel.newInstance(title, content, category, target, date);
                    sendContributionController.contribute(contributionViewModel);
                }
            });
        }

        private static ViewHolder createViewHolder (View view) {
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }
    }



    public interface IOnContributionInteraction {
        public void onContributeSuccess();
    }

    /**
     * Creates a contribution form fragment
     *
     * @return a contribution form fragment instance
     */
    public static ContributeFragment newInstance() {
        ContributeFragment fragment = new ContributeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.requiredViewState = MainActivityViewState.USER_STATUS_VIEW_STYLE;
        return fragment;
    }

    /**
     * Creates a contribution form fragment
     *
     * @param recoveryRequest the recovery respond the user
     *                        wants to respond to,
     *                        it will be used to set up default
     *                        form values.
     * @return a contribution form fragment instance
     */
    public static ContributeFragment newInstance(RecoveryRequest recoveryRequest) {
        ContributeFragment fragment = new ContributeFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ContributeFragment.REQUESTRECOVERY_ARG_NAME,
                recoveryRequest);
        fragment.setArguments(bundle);
        fragment.requiredViewState = MainActivityViewState.USER_STATUS_VIEW_STYLE;
        return fragment;
    }

}
