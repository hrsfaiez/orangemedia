package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment;


import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;

public interface IDeleteRecoveryRequestController {
    void deleteRecoveryRequest(RecoveryRequest recoveryRequest);
}
