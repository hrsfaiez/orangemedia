package tn.dev.orange.orangemedia.media.userinterface.control;


import android.view.View;

public interface IMediaViewHolder <TModel> {
    void updateView (View view);
    void setUpModel(TModel model);
}