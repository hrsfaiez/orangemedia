package tn.dev.orange.orangemedia.user.domain.datamodel;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * The user data model
 */
public final class User implements Parcelable {

    private String fistName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private String address;
    private Location location;

    public User(String fistName, String lastName, String username, String password, String email,
                String address) {
        this.fistName = fistName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.address = address;
        //TODO: omit this and use a user model builder
        location = new Location("Tunisia");
    }

    public User(Parcel in) {
        setFistName(in.readString());
        setLastName(in.readString());
        setUsername(in.readString());
        setPassword(in.readString());
        setEmail(in.readString());
        setAddress(in.readString());
    }

    public String getFistName() {
        return fistName;
    }

    private void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    private void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    private void setAddress(String address) {
        this.address = address;
    }

    private void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public String getName () {
        return getFistName() + " " + getLastName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getFistName());
        destination.writeString(getLastName());
        destination.writeString(getUsername());
        destination.writeString(getPassword());
        destination.writeString(getEmail());
        destination.writeString(getAddress());
    }

    public static final Parcelable.Creator<User> CREATOR
            = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) {
            return false;
        }
        if (getFistName().equals(((User) o).getFistName())
                && getLastName().equals(((User) o).getLastName())
                && getUsername().equals(((User) o).getUsername())
                && getPassword().equals(((User) o).getPassword())
                && getEmail().equals(((User) o).getEmail())
                && getAddress().equals(((User) o).getAddress())) {
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        String userString = getFistName() + "|" + getLastName();
        return userString;
    }
}
