package tn.dev.orange.orangemedia.user.dataaccess.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BSI on 12/06/2015.
 */
public class UserSQLite  extends SQLiteOpenHelper{
    private static final String TABLE_REPORTEURS = "table_reporteurs";
    private static final String COL_ID = "id";
    private static final String COL_LOGIN = "login";
    private static final String COL_PASSWORD = "password";
    private static final String COL_PHOTO = "photo";
    private static final String COL_NOM = "nom";
    private static final String COL_PRENOM = "prenom";
    private static final String COL_EMAIL = "email";
    private static final String COL_LOCALISATION = "localisation";

    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_REPORTEURS + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_LOGIN + " TEXT NOT NULL, "
            + COL_PASSWORD +" TEXT NOT NULL, " + COL_PHOTO + " TEXT NOT NULL, "  + COL_NOM + " TEXT NOT NULL, "
            + COL_PRENOM + " TEXT NOT NULL, "  + COL_EMAIL + " TEXT NOT NULL, "  + COL_LOCALISATION + " TEXT NOT NULL);";

    public UserSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on crée la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut faire ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
        //comme ça lorsque je change la version les id repartent de 0
        db.execSQL("DROP TABLE " + TABLE_REPORTEURS + ";");
        onCreate(db);
    }

}
