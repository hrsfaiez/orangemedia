package tn.dev.orange.orangemedia.user.userinterface.viewholder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.faiezdev.helloandro.R;

import tn.dev.orange.orangemedia.user.userinterface.control.IUserViewHolder;
import tn.dev.orange.orangemedia.user.userinterface.control.fragment.ISwitchToUserProfileEditController;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

/**
 * This class holds the view elements of the user
 * profile layout.
 * @<p>
 *      This is a helper class of the profile
 *      controller and should not be used outside
 *      that context.
 * </p>
 */
public class UserProfileViewHolder
        implements IUserViewHolder<User, ISwitchToUserProfileEditController> {
    /**
     * View elements of the profile layout.
     */
    private TextView firstNameTextView;
    private TextView lastNameTextView;
    private TextView usernameTextView;
    private TextView emailTextView;
    private TextView addressTextView;
    private ImageButton editProfileButton;

    /**
     * Initiates the view elements using a given
     * container view.
     * @param view the view of the profile container layout.
     */
    @Override
    public void updateView (View view) {
        firstNameTextView = (TextView) view.findViewById(R.id.userprofile_name_value_textview);
        lastNameTextView = (TextView) view.findViewById(R.id.userprofile_lastname_value_textview);
        usernameTextView = (TextView) view.findViewById(R.id.userprofile_username_value_textview);
        emailTextView = (TextView) view.findViewById(R.id.userprofile_email_value_textview);
        addressTextView = (TextView) view.findViewById(R.id.userprofile_address_value_textview);
        editProfileButton = (ImageButton) view.findViewById(R.id.userprofile_edit);
    }

    /**
     * Changes the view elements values to the
     * given user model values.
     * @param user the user model to be used
     *             to fill the view elements
     *             values.
     */
    @Override
    public void setUpModel(User user) {
        firstNameTextView.setText(user.getFistName());
        lastNameTextView.setText(user.getLastName());
        usernameTextView.setText(user.getUsername());
        emailTextView.setText(user.getEmail());
        addressTextView.setText(user.getAddress());
    }

    /**
     * Attaches an event listener to the profile edit event.
     * @param userProfileController the listener of the profile edit event.
     */
    @Override
    public void attachController(final ISwitchToUserProfileEditController userProfileController) {
        editProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userProfileController.switchToUserProfileEdit();
            }
        });
    }
}
