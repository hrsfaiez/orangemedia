package tn.dev.orange.orangemedia.user.domain.datamodel;


public class Location {
    private String name;

    public Location(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
