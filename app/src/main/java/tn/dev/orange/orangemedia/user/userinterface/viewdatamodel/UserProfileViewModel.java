package tn.dev.orange.orangemedia.user.userinterface.viewdatamodel;


import android.os.Parcel;
import android.os.Parcelable;

import tn.dev.orange.orangemedia.user.domain.datamodel.User;

/**
 * The User view model.
 * An empty attribute means the user do not want to modify it.
 */
public class UserProfileViewModel implements Parcelable {

    private String nom;
    private String prenom;
    private String username;
    private String password;
    private String email;
    private String address;

    public UserProfileViewModel(String nom, String prenom, String username, String password, String email, String address) {
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.email = email;
        this.address = address;
    }

    public UserProfileViewModel(Parcel in) {
        setNom(in.readString());
        setPrenom(in.readString());
        setUsername(in.readString());
        setPassword(in.readString());
        setEmail(in.readString());
        setAddress(in.readString());
    }

    public void clean () {
        nom = ((nom.trim().equals("")?null:nom));
        prenom = ((prenom.trim().equals("")?null:prenom));
        username = ((username.trim().equals("")?null:username));
        password = ((password.trim().equals("")?null:password));
        email = ((email.trim().equals("")?null:email));
        address = ((address.trim().equals("")?null:address));
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static final Parcelable.Creator<UserProfileViewModel> CREATOR
            = new Parcelable.Creator<UserProfileViewModel>() {
        public UserProfileViewModel createFromParcel(Parcel in) {
            return new UserProfileViewModel(in);
        }

        public UserProfileViewModel[] newArray(int size) {
            return new UserProfileViewModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 1;
    }

    @Override
    public void writeToParcel(Parcel destination, int i) {
        destination.writeString(getNom());
        destination.writeString(getPrenom());
        destination.writeString(getUsername());
        destination.writeString(getPassword());
        destination.writeString(getEmail());
        destination.writeString(getAddress());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof UserProfileViewModel))
            return false;
        if (getNom().equals(((UserProfileViewModel) o).getNom())
                && getPrenom().equals(((UserProfileViewModel) o).getPrenom())
                && getUsername().equals(((UserProfileViewModel) o).getUsername())
                && getPassword().equals(((UserProfileViewModel) o).getPassword())
                && getEmail().equals(((UserProfileViewModel) o).getEmail())
                && getAddress().equals(((UserProfileViewModel) o).getAddress()))
            return true;
        else return false;
    }

    public static UserProfileViewModel newInstance(String nom, String prenom, String username,
                                                   String password, String email, String address) {
        return new UserProfileViewModel(nom, prenom, username, password, email, address);
    }

    public User mapToModel () {
        return new User(nom, prenom, username, password, email, address);
    }
}
