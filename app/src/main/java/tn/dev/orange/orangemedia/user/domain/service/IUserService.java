package tn.dev.orange.orangemedia.user.domain.service;


import android.content.Context;

import tn.dev.orange.orangemedia.user.userinterface.control.fragment.IOnUserModelUpdateComplete;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;

/**
 * The user data model provider interface.
 * @<p>
 *     This interface should be implemented by
 *     a class that has an access to the data source.
 * </p>
 */
public interface IUserService {
    /**
     * Retrieve the current user data model.
     * @<p>
     *     The current user is the already logged user.
     *     Returns null when no user has been logged in.
     * </p>
     * @return the current user data model.
     */
    User getCurrentUser();

    /**
     * Updates the current user data model to a given
     * new data model.
     * @<p>
     *     The current user is the already logged user.
     * </p>
     * @param newUser the new user data model.
     */
    void updateCurrentUserModel(User newUser, Context context,
                                IOnUserModelUpdateComplete userModelUpdateCompleteListener);

}