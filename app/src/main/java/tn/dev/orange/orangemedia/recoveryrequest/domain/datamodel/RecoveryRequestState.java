package tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel;

/**
 * a recovery request_d state
 */
public enum RecoveryRequestState {
    /**
     * the recovery request_d is new
     */
    notSeen (0),
    /**
     * the recovery has been seen and responded seen by the user
     */
    seen_responded(1),
    /**
     * the recovery has been seen and not responded seen by the user
     */
    seen_notResponded(2);

    private Integer value;

    RecoveryRequestState(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
