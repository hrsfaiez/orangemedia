package tn.dev.orange.orangemedia.appmodel;

/***
 * This interface must be implemented by the mediator
 * of this activity to handle action bar events.
 */
public interface IOnActionBarInteraction {
    /**
     * Callback to the event triggered when the user wants to add a contribution
     */
    public void contribute();
    /**
     * Callback to the event triggered when the user wants to list the medias
     */
    public void viewMediaList();
    /**
     * Callback to the event triggered when the user wants to list the requests of the medias
     */
    public void viewRequestsList();
    /**
     * Callback to the event triggered when the user wants to view his profile
     */
    public void viewUserProfile();
}
