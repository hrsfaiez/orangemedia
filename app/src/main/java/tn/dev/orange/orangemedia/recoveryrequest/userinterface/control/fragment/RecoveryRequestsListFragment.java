package tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import tn.dev.orange.orangemedia.appmodel.viewstate.MainActivityViewState;
import tn.dev.orange.orangemedia.recoveryrequest.IRecoveryRequestModelProvider;
import tn.dev.orange.orangemedia.recoveryrequest.dataaccess.dataprovider.RecoveryRequestModelProvider;
import tn.dev.orange.orangemedia.user.domain.service.UserService;
import tn.dev.orange.orangemedia.recoveryrequest.domain.datamodel.RecoveryRequest;
import tn.dev.orange.orangemedia.user.domain.datamodel.User;
import tn.dev.orange.orangemedia.appmodel.connector.IFragmentAttachedListener;
import tn.dev.orange.orangemedia.recoveryrequest.userinterface.control.listadapter.RecoveryRequestsListAdapter;
import tn.dev.orange.orangemedia.user.domain.service.IUserService;

public class RecoveryRequestsListFragment extends ListFragment {

    //TODO:inject this
    IUserService userModelProvider = UserService.createUserModelProvider();
    //TODO:inject this
    IRecoveryRequestModelProvider recoveryRequestModelProvider
            = RecoveryRequestModelProvider.newInstance();

    private IOnRecoveryRequestSelectSelect mListener;

    private MainActivityViewState requiredViewState;

    public final MainActivityViewState getRequiredViewStyle() {
        return requiredViewState;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addRecoveryRequestsToList();
    }

    private void addRecoveryRequestsToList() {
        User currentUser = userModelProvider.getCurrentUser();

        List<RecoveryRequest> recoveryRequestsList
                = recoveryRequestModelProvider.getActiveRecoveryRequests(currentUser);

        RecoveryRequestsListAdapter recoveryRequestsListAdapter
                = RecoveryRequestsListAdapter.newInstance(getActivity(), recoveryRequestsList);
        setListAdapter(recoveryRequestsListAdapter);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((IFragmentAttachedListener) activity).getListener();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement IFragmentAttachedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        if (null != mListener) {
            RecoveryRequest recoveryRequest =
                    (RecoveryRequest) listView.getItemAtPosition(position);
            mListener.viewRecoveryRequest(recoveryRequest);
        }
    }

    /**
     * Creates a requests list fragment
     *
     * @return a requests list fragment fragment instance
     */
    public static RecoveryRequestsListFragment newInstance() {
        RecoveryRequestsListFragment fragment = new RecoveryRequestsListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.requiredViewState = MainActivityViewState.USER_STATUS_VIEW_STYLE;
        return fragment;
    }

}
